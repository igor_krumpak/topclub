package si.iitech.library.rest;

import android.util.Log;

public class RestResponce {

	private static final String TAG = RestResponce.class.getName();

	private int statusCode;
	private String data;
	private String extra;

	public RestResponce() {

	}

	public RestResponce(String data, int statusCode, String extra) {
		this.statusCode = statusCode;
		this.data = data;
		this.extra = extra;
		Log.i(TAG, "statuCode: " + statusCode + " data: " + data);
	}

	public int getStatusCode() {
		return statusCode;
	}

	public String getData() {
		return data;
	}

	public String getExtra() {
		return extra;
	}
}
