package si.iitech.topclub.object;

import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

/**
 * Created by igor on 3.6.2015.
 */
public class News implements Serializable {

    private String title;
    private String picture;
    private int id;
    private String description;
    private List<String> extraImages;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getPicture() {
        return picture;
    }

    public void setPicture(String picture) {
        this.picture = picture;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public List<String> getExtraImages() {
        return extraImages;
    }

    public void setExtraImages(List<String> extraImages) {
        this.extraImages = extraImages;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getDescription() {
        return description;
    }

    public static List<News> parseJsonNews(JSONArray array) {
        List<News> ekoList = new LinkedList<News>();
        for (int i = 0; i < array.length(); i++) {
            try {
                JSONObject object = array.getJSONObject(i);
                News eko = convertToNews(object);
                if (eko != null) {
                    ekoList.add(eko);
                }
            } catch (Exception e) {
                Log.d("Attraction Exception", e.toString());
            }
        }
        return ekoList;
    }

    private static News convertToNews(JSONObject object) throws JSONException{
        News news = new News();
        String title = object.getString("title");
        int id = object.getInt("blog_id");

        JSONArray imagesJsonArray;
        try {
            imagesJsonArray = object.getJSONArray("imgs");
        } catch (Exception e) {
            imagesJsonArray = new JSONArray();
        }

        String picture = "http://www.city-go.eu/image/" + object.getString("image");
        String descritpion = object.getString("description");

        if (id != 0 && title.length() != 0 && picture.length() != 0 ) {
            news.setTitle(title);
            news.setId(id);
            news.setPicture(picture);
            news.setDescription(descritpion);

            List<String> extraImages = new ArrayList<String>();
            for (int i = 0; i < imagesJsonArray.length(); i++) {
                extraImages.add(imagesJsonArray.getString(i));
            }
            news.setExtraImages(extraImages);

        } else {
            return null;
        }
        return news;
    }
}
