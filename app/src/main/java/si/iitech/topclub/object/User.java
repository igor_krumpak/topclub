package si.iitech.topclub.object;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import si.iitech.library.util.IITechUtil;
import si.iitech.topclub.common.Values;
import android.app.Activity;
import android.util.Log;

public class User {

	public static String	ID			= "ID";
	public static String	NAME		= "NAME";
	public static String	SURNAME		= "SURNAME";
	public static String	ADDRESS		= "ADDRESS";
	public static String	CITY		= "CITY";
	public static String	POSTCODE	= "POSTCODE";
	public static String	EMAIL		= "EMAIL";

	public static boolean login(Activity activity, String userData) {
		if (userData.equals("null")) {
			return false;
		} else {
			try {
				JSONArray userArray = new JSONArray(userData);
				JSONObject userJson = userArray.getJSONObject(0);

				String id = userJson.getString("customer_id");
				IITechUtil.saveInSharePreferences(activity, Values.APP_NAME, ID, id);

				String name = userJson.getString("firstname");
				IITechUtil.saveInSharePreferences(activity, Values.APP_NAME, NAME, name.trim());

				String surname = userJson.getString("lastname");
				IITechUtil.saveInSharePreferences(activity, Values.APP_NAME, SURNAME, surname.trim());

				String address = userJson.getString("payment_addres_1");
				IITechUtil.saveInSharePreferences(activity, Values.APP_NAME, ADDRESS, address.trim());

				String city = userJson.getString("payment_city");
				IITechUtil.saveInSharePreferences(activity, Values.APP_NAME, CITY, city.trim());

				String postcode = userJson.getString("payment_postcode");
				IITechUtil.saveInSharePreferences(activity, Values.APP_NAME, POSTCODE, postcode.trim());

				String email = userJson.getString("email");
				IITechUtil.saveInSharePreferences(activity, Values.APP_NAME, EMAIL, postcode.trim());

			} catch (JSONException e) {
				Log.d("JsonParser", e.toString());
				return false;
			}
			return true;
		}
	}

	public static boolean userExists(Activity activity) {
		if (IITechUtil.retriveFromSharedPreferences(activity, Values.APP_NAME, ID, "").equals("")) {
			return false;
		}
		return true;
	}

	public static void logut(Activity activity) {
		IITechUtil.saveInSharePreferences(activity, Values.APP_NAME, ID, "");
		IITechUtil.saveInSharePreferences(activity, Values.APP_NAME, NAME, "");
		IITechUtil.saveInSharePreferences(activity, Values.APP_NAME, SURNAME, "");
		IITechUtil.saveInSharePreferences(activity, Values.APP_NAME, CITY, "");
		IITechUtil.saveInSharePreferences(activity, Values.APP_NAME, POSTCODE, "");
		IITechUtil.saveInSharePreferences(activity, Values.APP_NAME, EMAIL, "");
	}

	public static String getUserNameAndSurname(Activity activity) {
		String name = IITechUtil.retriveFromSharedPreferences(activity, Values.APP_NAME, NAME, "");
		String surname = IITechUtil.retriveFromSharedPreferences(activity, Values.APP_NAME, SURNAME, "");
		return name + " " + surname;
	}

	public static String getUserID(Activity activity) {
		String id = IITechUtil.retriveFromSharedPreferences(activity, Values.APP_NAME, ID, "");
		return id;
	}

	public static String getUserName(Activity activity) {
		String name = IITechUtil.retriveFromSharedPreferences(activity, Values.APP_NAME, NAME, "Gost");
		return name;
	}

	public static String getUserSurname(Activity activity) {
		String name = IITechUtil.retriveFromSharedPreferences(activity, Values.APP_NAME, SURNAME, "");
		return name;
	}

	public static String getUserAddress(Activity activity) {
		String address = IITechUtil.retriveFromSharedPreferences(activity, Values.APP_NAME, ADDRESS, "");
		return address;
	}

	public static String getUserCity(Activity activity) {
		String city = IITechUtil.retriveFromSharedPreferences(activity, Values.APP_NAME, CITY, "");
		return city;
	}

	public static String getUserPostcode(Activity activity) {
		String postcode = IITechUtil.retriveFromSharedPreferences(activity, Values.APP_NAME, POSTCODE, "");
		return postcode;
	}

	public static String getUserEmail(Activity activity) {
		String email = IITechUtil.retriveFromSharedPreferences(activity, Values.APP_NAME, EMAIL, "");
		return email;
	}

}
