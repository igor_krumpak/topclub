package si.iitech.topclub.object;

import android.util.Log;


import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import si.iitech.library.util.IITechUtil;

/**
 * Created by igor on 3.6.2015.
 */
public class Attraction implements Serializable, Comparable<Attraction>{

    private String title;
    private String picture;
    private int id;
    private String location;
    private double lat;
    private double lon;
    private String content;
    private String description;
    private double distance;
    private List<String> extraImages;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getPicture() {
        return picture;
    }

    public void setPicture(String picture) {
        this.picture = picture;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public double getLat() {
        return lat;
    }

    public void setLat(double lat) {
        this.lat = lat;
    }

    public double getLon() {
        return lon;
    }

    public void setLon(double lon) {
        this.lon = lon;
    }

    public List<String> getExtraImages() {
        return extraImages;
    }

    public void setExtraImages(List<String> extraImages) {
        this.extraImages = extraImages;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getDescription() {
        return description;
    }

    public static List<Attraction> parseJsonAttractions(JSONArray array) {
        List<Attraction> attractionList = new LinkedList<Attraction>();
        for (int i = 0; i < array.length(); i++) {
            try {
                JSONObject object = array.getJSONObject(i);
                Attraction attraction = convertToAttraction(object);
                if (attraction != null) {
                    attractionList.add(attraction);
                }
            } catch (Exception e) {
                Log.d("Attraction Exception", e.toString());
            }
        }
        return attractionList;
    }

    private static Attraction convertToAttraction(JSONObject object) throws JSONException{
        Attraction attraction = new Attraction();
        String title = object.getString("title");
        int id = object.getInt("blog_id");
        String latitudeText = object.getString("latitude");
        String longtitudeText = object.getString("longitude");
        double latitude;
        double longtitude;

        JSONArray imagesJsonArray;
        try {
            imagesJsonArray = object.getJSONArray("imgs");
        } catch (Exception e) {
            imagesJsonArray = new JSONArray();
        }

        if(latitudeText.isEmpty()) {
            latitude = 0.0;
        } else {
            latitude = Double.parseDouble(latitudeText);
        }

        if(longtitudeText.isEmpty()) {
            longtitude = 0.0;
        } else {
            longtitude = Double.parseDouble(longtitudeText);
        }

        String location = object.getString("naslov");
        String picture = "http://www.city-go.eu/image/" + object.getString("image");
        String content = object.getString("content");
        String descritpion = object.getString("description");

        if (id != 0 && title.length() != 0 && picture.length() != 0 ) {
            attraction.setTitle(title);
            attraction.setId(id);
            attraction.setLat(latitude);
            attraction.setLon(longtitude);
            attraction.setLocation(location);
            attraction.setPicture(picture);
            attraction.setContent(content);
            attraction.setDescription(descritpion);

            List<String> extraImages = new ArrayList<String>();
            for (int i = 0; i < imagesJsonArray.length(); i++) {
                extraImages.add(imagesJsonArray.getString(i));
            }
            attraction.setExtraImages(extraImages);

        } else {
            return null;
        }

        return attraction;
    }

    public void setDistance(double distance) {
        this.distance = distance;
    }

    public double getDistance() {
        return distance;
    }

    @Override
    public int compareTo(Attraction c) {
        return Double.valueOf(distance).compareTo(c.distance);
    }
}
