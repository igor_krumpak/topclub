package si.iitech.topclub.object;

import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

import si.iitech.library.util.IITechUtil;
import si.iitech.topclub.common.RestPath;

public class Cupon implements Serializable, Comparable<Cupon> {

    private static final long serialVersionUID = 1L;
    private int id;
    private String picture;
    private String location;
    private String name;
    private String email;
    private String phone;
    private double lat;
    private double lon;
    private String description;
    private double price;
    private double specialPrice;
    private int model;
    private double distance;
    private String rulesOfTheCuponGame;

    private long startTime;
    private long endTime;
    private int deleteId;
    private int orderId;
    private String endDate;
    private List<String> extraImages;

    public static List<Cupon> parseJsonCupons(JSONArray array) {
        // parsaj samo tiste, ki vsebujejo location, name, email, phone, price
        // in special price
        List<Cupon> cuponList = new LinkedList<Cupon>();
        for (int i = 0; i < array.length(); i++) {
            try {
                JSONObject object = array.getJSONObject(i);
                Cupon cupon = convertToCupon(object);
                if (cupon != null) {
                    cuponList.add(cupon);
                }
            } catch (Exception e) {
                Log.d("Cupon Exception", e.toString());
            }
        }
        return cuponList;
    }

    private static Cupon convertToCupon(JSONObject object) throws JSONException {
        Cupon cupon = new Cupon();

        String name = object.getString("name");
        int id = object.getInt("product_id");
        int model = object.getInt("model");
        double price = object.getDouble("price");
        double specialPrice = object.getDouble("special_price");
        String picture = object.getString("image");
        long dateEnd = IITechUtil.convertTimeToMilis(object.getString("date_end"), "yyyy-MM-dd");
        Log.d("Time from json", object.getString("date_end"));
        String metaData = object.getString("meta_description");
        double latitude = object.getDouble("latitude");
        double longtitude = object.getDouble("longitude");
        String description = object.getString("description");

        JSONArray imagesJsonArray;
        try {
            imagesJsonArray = object.getJSONArray("imgs");
        } catch (Exception e) {
            imagesJsonArray = new JSONArray();
        }


        if (id != 0 && model != 0 && name.length() != 0 && picture.length() != 0 && dateEnd > 0.0) {
            cupon.setName(name);
            cupon.setPicture(RestPath.createPicturePath(picture));
            cupon.setSpecialPrice(specialPrice);
            cupon.setPrice(price);
            cupon.setId(id);
            cupon.setModel(model);
            cupon.setEndTime(dateEnd);
            formatLocation(object.getString("location").replace("*", ""), cupon);
            cupon.setRulesOfTheCuponGame(metaData);
            cupon.setLat(latitude);
            cupon.setLon(longtitude);
            cupon.setDescription(description);

            List<String> extraImages = new ArrayList<String>();
            for (int i = 0; i < imagesJsonArray.length(); i++) {
                extraImages.add(imagesJsonArray.getString(i));
            }
            cupon.setExtraImages(extraImages);

        } else {

            Log.i("Cupon", "cuppon not added id: " + id + " name: " + name + " picture: " + picture + " price: " + price + " dateEnd: " + dateEnd);
            return null;
        }
        return cupon;
    }

    private static void formatLocation(String location, Cupon cupon) {
        cupon.setLocation(location);
        // //Get address, city and zip
        // try {
        // String address = location.split(",")[1];
        // cupon.setAddress(address);
        //
        // String postcode = location.split(",")[2].split("\\s+")[1];
        // cupon.setPostcode(postcode);
        //
        // String city = location.split(",")[2].split("\\s")[2];
        // cupon.setCity(city);
        //
        // } catch(Exception e) {
        //
        // }
    }

    public static List<Cupon> parseJsonMyCupon(JSONArray array) {
        List<Cupon> list = new ArrayList<Cupon>();
        try {
            for (int i = 0; i < array.length(); i++) {
                JSONObject object = array.getJSONObject(i);
                Cupon cupon = convertToMyCupon(object);

                if (cupon != null) {
                    list.add(cupon);
                }
            }
        } catch (Exception e) {
            Log.d("parseJsonMyCupon", "Exception " + e.toString());
        }
        Collections.reverse(list);
        return list;
    }

    private static Cupon convertToMyCupon(JSONObject object) throws JSONException, Exception {

        Cupon myCupon = new Cupon();
        String name = object.getString("product_description_name");
        int orderId = object.getInt("order_id");

        double price = object.getDouble("price");
        double specialPrice = object.getDouble("special_price");
        String location = object.getString("location").split("[*]")[0] + " " + object.getString("location").split("[*]")[1];
        String rulesOfTheGame = object.getString("meta_description");
        int cuponId = object.getInt("order_id");
        int deleteId = object.getInt("order_product_id");
        long latitude = object.getLong("latitude");
        long longtittude = object.getLong("longitude");
        String endDate = object.getString("date_end");

        if (deleteId != 0 && cuponId != 0 && orderId != 0 && name.length() != 0 && price > 0.0 && location.length() != 0 && rulesOfTheGame.length() != 0) {
            myCupon.setName(name);
            myCupon.setSpecialPrice(specialPrice);
            myCupon.setPrice(price);
            myCupon.setOrderId(orderId);
            myCupon.setId(cuponId);
            myCupon.setLocation(location);
            myCupon.setEndDate(endDate);
            myCupon.setRulesOfTheCuponGame(rulesOfTheGame);
            myCupon.setLat(latitude);
            myCupon.setLon(longtittude);
            myCupon.setDeleteId(deleteId);
            return myCupon;
        } else {
            Log.i("Cupon", "cuppon not added");
            return null;
        }

    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getPicture() {
        return picture;
    }

    public void setPicture(String picture) {
        this.picture = picture;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public long getStartTime() {
        return startTime;
    }

    public void setStartTime(long startTime) {
        this.startTime = startTime;
    }

    public long getEndTime() {
        return endTime;
    }

    public void setEndTime(long endTime) {
        this.endTime = endTime;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public double getLat() {
        return lat;
    }

    public void setLat(double lat) {
        this.lat = lat;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public double getLon() {
        return lon;
    }

    public void setLon(double lon) {
        this.lon = lon;
    }

    public double getSpecialPrice() {
        return specialPrice;
    }

    public void setSpecialPrice(double specialPrice) {
        this.specialPrice = specialPrice;
    }

    public int getModel() {
        return model;
    }

    public void setModel(int model) {
        this.model = model;
    }

    public double getDistance() {
        return distance;
    }

    public void setDistance(double distance) {
        this.distance = distance;
    }

    @Override
    public int compareTo(Cupon c) {
        return Double.valueOf(distance).compareTo(c.distance);
    }

    public String getRulesOfTheCuponGame() {
        return rulesOfTheCuponGame;
    }

    public void setRulesOfTheCuponGame(String rulesOfTheCuponGame) {
        this.rulesOfTheCuponGame = rulesOfTheCuponGame;
    }

    public int getDeleteId() {
        return deleteId;
    }

    public void setDeleteId(int deleteId) {
        this.deleteId = deleteId;
    }

    public int getOrderId() {
        return orderId;
    }

    public void setOrderId(int orderId) {
        this.orderId = orderId;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    public static long getSerialversionuid() {
        return serialVersionUID;
    }

    public List<String> getExtraImages() {
        return extraImages;
    }

    public void setExtraImages(List<String> extraImages) {
        this.extraImages = extraImages;
    }
}
