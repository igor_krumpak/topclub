package si.iitech.topclub.adapter;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.nostra13.universalimageloader.core.ImageLoader;

import java.util.List;

import si.iitech.topclub.R;
import si.iitech.topclub.common.Formatter;
import si.iitech.topclub.object.Cupon;

public class CuponAdapter extends BaseAdapter {

    private Context context;
    private List<Cupon> list;

    public CuponAdapter(Context context, List<Cupon> list) {
        this.context = context;
        this.list = list;
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int index) {
        return list.get(index);
    }

    @Override
    public long getItemId(int arg0) {
        return 0;
    }

    private class CuponHolder {
        TextView specialPrice;
        TextView oldPrice;
        TextView title;
        TextView oldPriceTitle;
        ImageView picture;
    }

    @Override
    public View getView(int index, View convertView, ViewGroup view) {
        Cupon cupon = list.get(index);
        CuponHolder holder = null;

        LayoutInflater inflater = (LayoutInflater) this.context.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
        if (null == convertView) {
            convertView = inflater.inflate(R.layout.cupon_adapter, null);
            holder = new CuponHolder();
            holder.specialPrice = (TextView) convertView.findViewById(R.id.textView_special_price);
            holder.oldPrice = (TextView) convertView.findViewById(R.id.textView_old_price);
            holder.oldPriceTitle = (TextView) convertView.findViewById(R.id.textView_old_price_title);
            holder.title = (TextView) convertView.findViewById(R.id.textView_title);
            holder.picture = (ImageView) convertView.findViewById(R.id.imageView_picture);
            convertView.setTag(holder);
        } else {
            holder = (CuponHolder) convertView.getTag();
        }

        holder.title.setText(cupon.getName());
        // TODO format

        Formatter.formatCuponOutput(context, holder.oldPrice, holder.specialPrice, cupon, holder.oldPriceTitle, "");

        ImageLoader.getInstance().displayImage(cupon.getPicture(), holder.picture);

        return convertView;
    }

}
