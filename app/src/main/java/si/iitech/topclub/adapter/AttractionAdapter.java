package si.iitech.topclub.adapter;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.nostra13.universalimageloader.core.ImageLoader;

import java.util.List;

import si.iitech.topclub.R;
import si.iitech.topclub.object.Attraction;

/**
 * Created by igor on 3.6.2015.
 */
public class AttractionAdapter extends BaseAdapter {

    private Context context;
    private List<Attraction> list;

    public AttractionAdapter(Context context, List<Attraction> list) {
        this.context = context;
        this.list = list;
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int index) {
        return list.get(index);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    private class AttractionHolder {
        TextView title;
        ImageView picture;
    }

    @Override
    public View getView(int index, View convertView, ViewGroup parent) {

        Attraction attraction = list.get(index);
        AttractionHolder holder = null;

        LayoutInflater inflater = (LayoutInflater) this.context.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
        if (null == convertView) {
            convertView = inflater.inflate(R.layout.attraction_adapter, null);
            holder = new AttractionHolder();
            holder.title = (TextView) convertView.findViewById(R.id.textView_title);
            holder.picture = (ImageView) convertView.findViewById(R.id.imageView_picture);
            convertView.setTag(holder);
        } else {
            holder = (AttractionHolder) convertView.getTag();
        }

        holder.title.setText(attraction.getTitle());

        // TODO format
        ImageLoader.getInstance().displayImage(attraction.getPicture(), holder.picture);
        return convertView;
    }
}
