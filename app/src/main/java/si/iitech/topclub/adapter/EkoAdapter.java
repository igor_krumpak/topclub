package si.iitech.topclub.adapter;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.nostra13.universalimageloader.core.ImageLoader;

import java.util.List;

import si.iitech.topclub.R;
import si.iitech.topclub.object.Attraction;
import si.iitech.topclub.object.Eko;

/**
 * Created by igor on 3.6.2015.
 */
public class EkoAdapter extends BaseAdapter {

    private Context context;
    private List<Eko> list;

    public EkoAdapter(Context context, List<Eko> list) {
        this.context = context;
        this.list = list;
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int index) {
        return list.get(index);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    private class EkoHolder {
        TextView title;
        ImageView picture;
    }

    @Override
    public View getView(int index, View convertView, ViewGroup parent) {

        Eko eko = list.get(index);
        EkoHolder holder = null;

        LayoutInflater inflater = (LayoutInflater) this.context.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
        if (null == convertView) {
            convertView = inflater.inflate(R.layout.attraction_adapter, null);
            holder = new EkoHolder();
            holder.title = (TextView) convertView.findViewById(R.id.textView_title);
            holder.picture = (ImageView) convertView.findViewById(R.id.imageView_picture);
            convertView.setTag(holder);
        } else {
            holder = (EkoHolder) convertView.getTag();
        }

        holder.title.setText(eko.getTitle());

        // TODO format
        ImageLoader.getInstance().displayImage(eko.getPicture(), holder.picture);
        return convertView;
    }
}
