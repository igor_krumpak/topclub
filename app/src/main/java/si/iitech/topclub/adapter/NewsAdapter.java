package si.iitech.topclub.adapter;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.nostra13.universalimageloader.core.ImageLoader;

import java.util.List;

import si.iitech.topclub.R;
import si.iitech.topclub.object.Eko;
import si.iitech.topclub.object.News;

/**
 * Created by igor on 3.6.2015.
 */
public class NewsAdapter extends BaseAdapter {

    private Context context;
    private List<News> list;

    public NewsAdapter(Context context, List<News> list) {
        this.context = context;
        this.list = list;
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int index) {
        return list.get(index);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    private class NewsHolder {
        TextView title;
        ImageView picture;
        TextView description;
    }

    @Override
    public View getView(int index, View convertView, ViewGroup parent) {

        News news = list.get(index);
        NewsHolder holder = null;

        LayoutInflater inflater = (LayoutInflater) this.context.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
        if (null == convertView) {
            convertView = inflater.inflate(R.layout.news_adapter, null);
            holder = new NewsHolder();
            holder.title = (TextView) convertView.findViewById(R.id.textView_news_title);
            holder.picture = (ImageView) convertView.findViewById(R.id.imageView_news_picture);
            holder.description = (TextView) convertView.findViewById(R.id.textView_news_description);
            convertView.setTag(holder);
        } else {
            holder = (NewsHolder) convertView.getTag();
        }
        holder.title.setText(news.getTitle());
        ImageLoader.getInstance().displayImage(news.getPicture(), holder.picture);
        holder.description.setText(createDescription(news.getDescription()));
        return convertView;
    }

    private String createDescription(String description) {
        String formatedString = android.text.Html.fromHtml(description).toString();
        if(formatedString.length() < 100) {
            return formatedString + "...";
        }
        return formatedString.substring(0, 100) + "...";
    }
}
