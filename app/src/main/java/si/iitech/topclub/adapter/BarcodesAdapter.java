package si.iitech.topclub.adapter;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import si.iitech.topclub.R;
import si.iitech.topclub.barcode.Barcode;

public class BarcodesAdapter extends BaseAdapter {

    private Context context;
    private List<Barcode> list;

    public BarcodesAdapter(Context context, List<Barcode> list) {
        this.context = context;
        this.list = list;
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int index) {
        return list.get(index);
    }

    @Override
    public long getItemId(int arg0) {
        return 0;
    }

    private class BarcodeHolder {
        TextView textViewBarcodeName;
        TextView textViewBarcodeShop;
        ImageView imageBarcodeShop;
    }

    @Override
    public View getView(int index, View convertView, ViewGroup view) {
        final Barcode barcode = list.get(index);
        BarcodeHolder holder = null;

        LayoutInflater inflater = (LayoutInflater) this.context.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
        if (null == convertView) {
            convertView = inflater.inflate(R.layout.barcodes_adapter, null);
            holder = new BarcodeHolder();
            holder.textViewBarcodeName = (TextView) convertView.findViewById(R.id.textView_barcode_name);
            holder.textViewBarcodeShop = (TextView) convertView.findViewById(R.id.textView_barcode_shop);
            holder.imageBarcodeShop = (ImageView) convertView.findViewById(R.id.imageView_barcode_picture);
            convertView.setTag(holder);
        } else {
            holder = (BarcodeHolder) convertView.getTag();
        }

        holder.textViewBarcodeName.setText(barcode.getTitle());
        holder.textViewBarcodeShop.setText(barcode.getShop());
        holder.imageBarcodeShop.setImageDrawable(barcode.getImage(context));

        return convertView;
    }

}
