package si.iitech.topclub.adapter;

import java.util.List;

import si.iitech.topclub.R;
import si.iitech.topclub.common.Formatter;
import si.iitech.topclub.common.Values;
import si.iitech.topclub.fragments.ChangeFragment;
import si.iitech.topclub.fragments.TopClubFragment;
import si.iitech.topclub.object.Cupon;
import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.TextView;

public class MyCuponAdapter extends BaseAdapter {

	private Context			context;
	private List<Cupon>		list;
	private ChangeFragment	notifier;

	public MyCuponAdapter(Context context, List<Cupon> list, ChangeFragment notifier) {
		this.context = context;
		this.list = list;
		this.notifier = notifier;
	}

	@Override
	public int getCount() {
		return list.size();
	}

	@Override
	public Object getItem(int index) {
		return list.get(index);
	}

	@Override
	public long getItemId(int arg0) {
		return 0;
	}

	private class CuponHolder {
		TextView	specialPrice;
		TextView	oldPrice;
		TextView	name;
		TextView	orderId;
	}

	@Override
	public View getView(int index, View convertView, ViewGroup view) {
		final Cupon cupon = list.get(index);
		CuponHolder holder = null;

		LayoutInflater inflater = (LayoutInflater) this.context.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
		if (null == convertView) {
			convertView = inflater.inflate(R.layout.my_cupon_adapter, null);
			holder = new CuponHolder();
			holder.specialPrice = (TextView) convertView.findViewById(R.id.textView_cupon_purchase_special_price);
			holder.oldPrice = (TextView) convertView.findViewById(R.id.textView_cupon_purchase_price);
			holder.name = (TextView) convertView.findViewById(R.id.textView_cupon_purchase_name);
			holder.orderId = (TextView) convertView.findViewById(R.id.textView_cupon_serial_number);
			convertView.setTag(holder);
		} else {
			holder = (CuponHolder) convertView.getTag();
		}

		Formatter.formatCuponOutput(context, holder.oldPrice, holder.specialPrice, cupon, null, context.getResources().getString(R.string.procent_text) + " ");
		holder.orderId.setText(cupon.getOrderId() + "");
		holder.name.setText(cupon.getName());

		
		return convertView;
	}

}
