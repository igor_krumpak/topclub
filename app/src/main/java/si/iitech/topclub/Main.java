package si.iitech.topclub;


import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.util.Base64;
import android.util.Log;

import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import si.iitech.library.activity.IITechActivity;
import si.iitech.topclub.barcode.BarcodeList;
import si.iitech.topclub.barcode.NewBarcodeStepOne;
import si.iitech.topclub.barcode.NewBarcodeStepTwo;
import si.iitech.topclub.fragments.AttractionDetails;
import si.iitech.topclub.fragments.AttractionList;
import si.iitech.topclub.fragments.ChangeFragment;
import si.iitech.topclub.fragments.CuponDetails;
import si.iitech.topclub.fragments.CuponFilterNearby;
import si.iitech.topclub.fragments.CuponFilterOnTheWay;
import si.iitech.topclub.fragments.CuponList;
import si.iitech.topclub.fragments.DiffrentCuponList;
import si.iitech.topclub.fragments.EkoDetails;
import si.iitech.topclub.fragments.EkoList;
import si.iitech.topclub.fragments.Login;
import si.iitech.topclub.fragments.Map;
import si.iitech.topclub.fragments.Menu;
import si.iitech.topclub.fragments.MyCuponDetails;
import si.iitech.topclub.fragments.MyCuponList;
import si.iitech.topclub.fragments.NewsDetails;
import si.iitech.topclub.fragments.NewsList;
import si.iitech.topclub.fragments.Registration;
import si.iitech.topclub.fragments.TopClubFragment;

public class Main extends IITechActivity implements ChangeFragment {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);



        setContentView(R.layout.main);
        selectFragment();
        callTracker();



    }

    private void callTracker() {
        Tracker t = ((TopClubApplication) this.getApplication()).getTracker();
        t.send(new HitBuilders.EventBuilder().setCategory("Mobile app").setAction("Intro").setLabel("Intro").build());

    }

    private void selectFragment() {
        // if (User.userExists(this)) {
        changeFragment(TopClubFragment.MENU, null, true);
        // } else {
        // changeFragment(TopClubFragment.LOGIN, null, true);
        // }
    }

    // TODO move to library
    protected void changeFragment(Fragment fragment, int fragmenentLayout, boolean remove) {
        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.setTransition(FragmentTransaction.TRANSIT_ENTER_MASK);
        if (remove) {
            fragmentTransaction.add(fragmenentLayout, fragment);
        } else {
            //fragmentTransaction.add(fragmenentLayout, fragment);
            fragmentTransaction.add(fragmenentLayout, fragment).addToBackStack(null);
        }
        fragmentTransaction.commitAllowingStateLoss();
    }

    @Override
    protected void initGraphicComponents() {

    }

    @Override
    public void changeFragment(TopClubFragment fragment, Bundle bundle, boolean remove) {
        Fragment temp = null;
        switch (fragment) {
            case LOGIN:
                temp = new Login();
                break;
            case MENU:
                temp = new Menu();
                break;
            case REGISTRATION:
                temp = new Registration();
                break;
            case CUPON_FILTER_NEARBY:
                temp = new CuponFilterNearby();
                break;
            case CUPON_FILTER_ON_THE_WAY:
                temp = new CuponFilterOnTheWay();
                break;
            case CUPON_LIST:
                temp = new CuponList();
                break;
            case CUPON_DETAILS:
                temp = new CuponDetails();
                break;
            case MAP:
                temp = new Map();
                break;
            case MY_CUPONS:
                temp = new MyCuponList();
                break;
            case CUPON_CODE:
                temp = new MyCuponDetails();
                break;
            case NEW_BAR_CODE_STEP_ONE:
                temp = new NewBarcodeStepOne();
                break;
            case NEW_BAR_CODE_STEP_TWO:
                temp = new NewBarcodeStepTwo();
                break;
            case BAR_CODE_LIST:
                temp = new BarcodeList();
                break;
            case ATTRACTION_LIST:
                temp = new AttractionList();
                break;
            case ATTRACTION_DETAILS:
                temp = new AttractionDetails();
                break;
            case EKO_LIST:
                temp = new EkoList();
                break;
            case EKO_DETAILS:
                temp = new EkoDetails();
                break;
            case NEWS_LIST:
                temp = new NewsList();
                break;
            case NEWS_DETAILS:
                temp = new NewsDetails();
                break;
            case DIFFRENT_CUPON_LIST:
                temp = new DiffrentCuponList();
                break;
        }
        if (bundle != null) {
            temp.setArguments(bundle);
        }
        changeFragment(temp, R.id.ViewSwitcher, remove);
    }

    @Override
    public void popFragment() {
        new Handler().post(new Runnable() {
            @Override
            public void run() {
                FragmentManager fragmentManager = getSupportFragmentManager();
                fragmentManager.popBackStack();
            }
        });
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    @Override
    public void startComercial() {
        Intent intent = new Intent(this, ComercialPlayer.class);
        startActivity(intent);

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        for (Fragment fragment : getSupportFragmentManager().getFragments()) {
            fragment.onActivityResult(requestCode, resultCode, data);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(android.view.Menu menu) {
        return super.onCreateOptionsMenu(menu);
    }
}
