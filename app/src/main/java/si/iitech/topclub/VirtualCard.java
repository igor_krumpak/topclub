package si.iitech.topclub;


import android.content.ContentResolver;
import android.content.ContentUris;
import android.content.ContentValues;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.Loader;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;

import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.assist.FailReason;
import com.nostra13.universalimageloader.core.listener.ImageLoadingListener;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.OutputStream;

import si.iitech.library.activity.IITechActivity;
import si.iitech.library.rest.RestCall;
import si.iitech.library.rest.RestEnum;
import si.iitech.library.rest.RestResponce;
import si.iitech.topclub.common.RestPath;
import si.iitech.topclub.common.Values;
import si.iitech.topclub.object.User;

public class VirtualCard extends IITechActivity implements LoaderManager.LoaderCallbacks<RestResponce> {

    private ImageView virtualCardImageView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.virtual_card);
        virtualCardImageView = (ImageView) findViewById(R.id.virtual_card_image_view);

        String retrivedImage = retriveFromSharedPreferences("image", "");
        if (retrivedImage.isEmpty()) {
            callRest();
        } else {
            ImageLoader.getInstance().displayImage(retrivedImage, virtualCardImageView, new ImageLoadingListener() {
                @Override
                public void onLoadingStarted(String imageUri, View view) {

                }

                @Override
                public void onLoadingFailed(String imageUri, View view, FailReason failReason) {
                    callRest();
                }

                @Override
                public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {

                }

                @Override
                public void onLoadingCancelled(String imageUri, View view) {

                }
            });
        }


    }

    private void callRest() {
        String path = "http://www.city-go.eu/restful.php?type=returnKartico&";
        StringBuilder builder = new StringBuilder();
        builder.append(path);
        builder.append("user_id=");
        builder.append(User.getUserID(this));
        builder.append("&hash=");
        builder.append(Values.KEY);
        Bundle args = new Bundle();
        Log.i("login", builder.toString());
        args.putParcelable(RestCall.URI_REST, Uri.parse(RestPath.makeStringUrlFriendly(builder.toString())));
        getSupportLoaderManager().initLoader(RestCall.NALOZI_REST, args, this);
    }


    @Override
    protected void initGraphicComponents() {

    }

    @Override
    public Loader<RestResponce> onCreateLoader(int id, Bundle args) {
        Uri url = args.getParcelable(RestCall.URI_REST);
        return new RestCall(this, RestEnum.GET, url, "", "");
    }

    @Override
    public void onLoadFinished(Loader<RestResponce> loader, RestResponce responce) {
        if (responce.getStatusCode() == RestCall.httpStatusOK) {
            try {
                JSONObject jsonObject = new JSONObject(responce.getData());
                String imgPath = jsonObject.getString("img");
                ImageLoader.getInstance().displayImage(imgPath, virtualCardImageView);

            } catch (JSONException e) {
                e.printStackTrace();
            }
        } else {
            Toast.makeText(getApplicationContext(), getResources().getString(R.string.error_no_connection), Toast.LENGTH_LONG).show();
        }
        getLoaderManager().destroyLoader(RestCall.NALOZI_REST);
    }

    @Override
    public void onLoaderReset(Loader<RestResponce> loader) {

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.virtual_card, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_save_virtual_card:
                try {
                    Bitmap bitmap = ((BitmapDrawable) virtualCardImageView.getDrawable()).getBitmap();
                    String path = insertImage(this.getContentResolver(), bitmap, "VirtualCard", "");
                    saveInSharedPreferences("image", path);
                    Toast.makeText(getApplicationContext(), getResources().getString(R.string.success_saving_image) + "\n" + path, Toast.LENGTH_LONG).show();
                } catch (Exception e) {
                    Toast.makeText(getApplicationContext(), getResources().getString(R.string.error_saving_image), Toast.LENGTH_LONG).show();
                }
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    /**
     * A copy of the Android internals  insertImage method, this method populates the
     * meta data with DATE_ADDED and DATE_TAKEN. This fixes a common problem where media
     * that is inserted manually gets saved at the end of the gallery (because date is not populated).
     *
     * @see android.provider.MediaStore.Images.Media#insertImage(ContentResolver, Bitmap, String, String)
     */
    private String insertImage(ContentResolver cr, Bitmap source, String title, String description) {

        ContentValues values = new ContentValues();
        values.put(MediaStore.Images.Media.TITLE, title);
        values.put(MediaStore.Images.Media.DISPLAY_NAME, title);
        values.put(MediaStore.Images.Media.DESCRIPTION, description);
        values.put(MediaStore.Images.Media.MIME_TYPE, "image/jpeg");
        // Add the date meta data to ensure the image is added at the front of the gallery
        values.put(MediaStore.Images.Media.DATE_ADDED, System.currentTimeMillis());
        values.put(MediaStore.Images.Media.DATE_TAKEN, System.currentTimeMillis());

        Uri url = null;
        String stringUrl = null;    /* value to be returned */

        try {
            url = cr.insert(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, values);

            if (source != null) {
                OutputStream imageOut = cr.openOutputStream(url);
                try {
                    source.compress(Bitmap.CompressFormat.JPEG, 50, imageOut);
                } finally {
                    imageOut.close();
                }

                long id = ContentUris.parseId(url);
                // Wait until MINI_KIND thumbnail is generated.
                Bitmap miniThumb = MediaStore.Images.Thumbnails.getThumbnail(cr, id, MediaStore.Images.Thumbnails.MINI_KIND, null);
                // This is for backward compatibility.
                storeThumbnail(cr, miniThumb, id, 50F, 50F, MediaStore.Images.Thumbnails.MICRO_KIND);
            } else {
                cr.delete(url, null, null);
                url = null;
            }
        } catch (Exception e) {
            if (url != null) {
                cr.delete(url, null, null);
                url = null;
            }
        }

        if (url != null) {
            stringUrl = url.toString();
        }

        return stringUrl;
    }

    /**
     * A copy of the Android internals StoreThumbnail method, it used with the insertImage to
     * populate the android.provider.MediaStore.Images.Media#insertImage with all the correct
     * meta data. The StoreThumbnail method is private so it must be duplicated here.
     *
     * @see android.provider.MediaStore.Images.Media (StoreThumbnail private method)
     */
    private Bitmap storeThumbnail(
            ContentResolver cr,
            Bitmap source,
            long id,
            float width,
            float height,
            int kind) {

        // create the matrix to scale it
        Matrix matrix = new Matrix();

        float scaleX = width / source.getWidth();
        float scaleY = height / source.getHeight();

        matrix.setScale(scaleX, scaleY);

        Bitmap thumb = Bitmap.createBitmap(source, 0, 0,
                source.getWidth(),
                source.getHeight(), matrix,
                true
        );

        ContentValues values = new ContentValues(4);
        values.put(MediaStore.Images.Thumbnails.KIND, kind);
        values.put(MediaStore.Images.Thumbnails.IMAGE_ID, (int) id);
        values.put(MediaStore.Images.Thumbnails.HEIGHT, thumb.getHeight());
        values.put(MediaStore.Images.Thumbnails.WIDTH, thumb.getWidth());

        Uri url = cr.insert(MediaStore.Images.Thumbnails.EXTERNAL_CONTENT_URI, values);

        try {
            OutputStream thumbOut = cr.openOutputStream(url);
            thumb.compress(Bitmap.CompressFormat.JPEG, 100, thumbOut);
            thumbOut.close();
            return thumb;
        } catch (FileNotFoundException ex) {
            return null;
        } catch (IOException ex) {
            return null;
        }
    }

    /**

     @Override public Loader<RestResponce> onCreateLoader(int id, Bundle args) {
     Uri url = args.getParcelable(RestCall.URI_REST);
     return new RestCall(this, RestEnum.GET, url, "");
     }

     @Override public void onLoadFinished(Loader<RestResponce> loader, RestResponce responce) {
     if (responce.getStatusCode() == RestCall.httpStatusOK) {
     try {
     JSONObject jsonObject = new JSONObject(responce.getData());
     String imgPath = jsonObject.getString("img");
     ImageLoader.getInstance().displayImage(imgPath, virtualCardImageView);

     } catch (JSONException e) {
     e.printStackTrace();
     }
     } else {
     Toast.makeText(getApplicationContext(), getResources().getString(R.string.error_no_connection), Toast.LENGTH_LONG).show();
     }
     getLoaderManager().destroyLoader(RestCall.NALOZI_REST);
     }

     @Override public void onLoaderReset(Loader<RestResponce> loader) {

     }

     **/
}
