package si.iitech.topclub.fragments;

import android.app.Activity;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.Loader;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.List;

import si.iitech.library.fragment.IITechFragment;
import si.iitech.library.rest.RestCall;
import si.iitech.library.rest.RestEnum;
import si.iitech.library.rest.RestResponce;
import si.iitech.library.util.IITechUtil;
import si.iitech.topclub.R;
import si.iitech.topclub.common.Arial;
import si.iitech.topclub.common.RestPath;
import si.iitech.topclub.common.Values;

public class Registration extends IITechFragment implements LoaderManager.LoaderCallbacks<RestResponce>, OnClickListener, OnItemSelectedListener {

    private EditText editTextName = null;
    private EditText editTextSurname = null;
    private EditText editTextEmail = null;
    private EditText editTextAddress = null;
    private EditText editTextCity = null;
    private EditText editTextPostalCode = null;
    private Spinner spinnerCountry = null;
    private Spinner spinnerObcine = null;
    private EditText editTextPassword = null;
    private Button buttonRegister = null;
    private ChangeFragment notifier = null;
    private String country = null;
    private String categoryId = null;
    private TextView emailInUse = null;
    private List<String> countries = null;
    private List<String> obcineName = null;
    private List<String> obcineId = null;
    private TextView registrationObcineHint = null;

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            notifier = (ChangeFragment) activity;
        } catch (ClassCastException e) {
        }
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.registration, container, false);

        editTextName = (EditText) view.findViewById(R.id.registration_name);
        editTextName.setTypeface(Arial.getInstance(getActivity()).getTypeFace());
        editTextSurname = (EditText) view.findViewById(R.id.registration_surname);
        editTextSurname.setTypeface(Arial.getInstance(getActivity()).getTypeFace());
        editTextEmail = (EditText) view.findViewById(R.id.registration_email);
        editTextEmail.setTypeface(Arial.getInstance(getActivity()).getTypeFace());
        editTextAddress = (EditText) view.findViewById(R.id.registration_address);
        editTextAddress.setTypeface(Arial.getInstance(getActivity()).getTypeFace());
        editTextCity = (EditText) view.findViewById(R.id.registration_city);
        editTextCity.setTypeface(Arial.getInstance(getActivity()).getTypeFace());
        editTextPostalCode = (EditText) view.findViewById(R.id.registration_postcode);
        editTextPostalCode.setTypeface(Arial.getInstance(getActivity()).getTypeFace());

        registrationObcineHint = (TextView)view.findViewById(R.id.registration_obcine_hint);
        registrationObcineHint.setTypeface(Arial.getInstance(getActivity()).getTypeFace());

        spinnerCountry = (Spinner) view.findViewById(R.id.registration_countries_spinner);
        spinnerCountry.setOnItemSelectedListener(new OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> arg0, View view, int arg2, long arg3) {
                Long position = arg0.getItemIdAtPosition(arg2) + 1;
                country = position.toString();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        countries = populateCountries();
        ArrayAdapter<String> adapterCountries = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_item, countries);
        adapterCountries.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerCountry.setAdapter(adapterCountries);
        spinnerCountry.setSelection(189);

        obcineName = populateObcineName();
        obcineId = populateObcineId();

        spinnerObcine = (Spinner) view.findViewById(R.id.registration_obcine_spinner);
        spinnerObcine.setOnItemSelectedListener(new OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> arg0, View view, int arg2, long arg3) {
                categoryId = obcineId.get(arg2);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        ArrayAdapter<String> adapterObcine = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_item, obcineName);
        adapterObcine.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerObcine.setAdapter(adapterObcine);
        spinnerObcine.setSelection(0);

        editTextPassword = (EditText) view.findViewById(R.id.registration_password);
        editTextPassword.setTypeface(Arial.getInstance(getActivity()).getTypeFace());
        buttonRegister = (Button) view.findViewById(R.id.registration_submit);
        buttonRegister.setTypeface(Arial.getInstance(getActivity()).getTypeFace());
        buttonRegister.setOnClickListener(this);
        emailInUse = (TextView) view.findViewById(R.id.registration_textview_wrong_email);
        emailInUse.setTypeface(Arial.getInstance(getActivity()).getTypeFace());
        emailInUse.setVisibility(View.INVISIBLE);

        return view;
    }

    private List<String> populateCountries() {
        List<String> countries = null;
        InputStream jsonStream = this.getResources().openRawResource(R.raw.countries);
        try {
            JSONArray jsonCountries = new JSONArray(convertStreamToString(jsonStream));
            countries = new ArrayList<String>();
            for (int i = 0; i < jsonCountries.length(); i++) {
                JSONObject jsonCountry = jsonCountries.getJSONObject(i);
                countries.add(jsonCountry.getString("name"));
            }
            return countries;
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return countries;
    }

    private List<String> populateObcineName() {
        List<String> list = null;
        InputStream jsonStream = this.getResources().openRawResource(R.raw.obcine);
        try {
            JSONArray jsonObcine = new JSONArray(convertStreamToString(jsonStream));
            list = new ArrayList<String>();
            for (int i = 0; i < jsonObcine.length(); i++) {
                JSONObject jsonCountry = jsonObcine.getJSONObject(i);
                list.add(jsonCountry.getString("name"));
            }
            return list;
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return list;
    }

    private List<String> populateObcineId() {
        List<String> list = null;
        InputStream jsonStream = this.getResources().openRawResource(R.raw.obcine);
        try {
            JSONArray jsonObcine = new JSONArray(convertStreamToString(jsonStream));
            list = new ArrayList<String>();
            for (int i = 0; i < jsonObcine.length(); i++) {
                JSONObject jsonCountry = jsonObcine.getJSONObject(i);
                list.add(jsonCountry.getString("category_id"));
            }
            return list;
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return list;
    }


    private String convertStreamToString(InputStream is) {
        ByteArrayOutputStream oas = new ByteArrayOutputStream();
        copyStream(is, oas);
        String t = oas.toString();
        try {
            oas.close();
            oas = null;
        } catch (IOException e) {
            e.printStackTrace();
        }
        return t;
    }

    private void copyStream(InputStream is, OutputStream os) {
        final int buffer_size = 1024;
        try {
            byte[] bytes = new byte[buffer_size];
            for (; ; ) {
                int count = is.read(bytes, 0, buffer_size);
                if (count == -1)
                    break;
                os.write(bytes, 0, count);
            }
        } catch (Exception ex) {
        }
    }

    @Override
    protected void initGraphicComponents() {
        // TODO Auto-generated method stub

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.registration_submit:
                if (IITechUtil.checkValues(editTextName, null, getResources().getString(R.string.validation_name), null)
                        && IITechUtil.checkValues(editTextSurname, null, getResources().getString(R.string.validation_surname), null)
                        && IITechUtil.checkValues(editTextEmail, Values.EMAIL_PATTERN, getResources().getString(R.string.validation_email), getResources()
                        .getString(R.string.validation_wrong_email))
                        && IITechUtil.checkValues(editTextAddress, null, getResources().getString(R.string.validation_address), null)
                        && IITechUtil.checkValues(editTextCity, null, getResources().getString(R.string.validation_city), null)
                        && IITechUtil.checkValues(editTextPostalCode, Values.NUMBERS_PATTERN, getResources().getString(R.string.validation_posteCode),
                        getResources().getString(R.string.validation_wrong_posteCode))
                        && IITechUtil.checkValues(editTextPassword, null, getResources().getString(R.string.validation_password), null)) {
                    String name = editTextName.getText().toString();
                    String surname = editTextSurname.getText().toString();
                    String email = editTextEmail.getText().toString();
                    String address = editTextAddress.getText().toString();
                    String city = editTextCity.getText().toString();
                    String postcode = editTextPostalCode.getText().toString();
                    String password = editTextPassword.getText().toString();

                    callRest(name, surname, email, address, city, postcode, country, categoryId, password);
                    break;
                }

        }

    }

    @Override
    public Loader<RestResponce> onCreateLoader(int arg0, Bundle arg1) {
        Uri url = arg1.getParcelable(RestCall.URI_REST);
        return new RestCall(this.getActivity(), RestEnum.GET, url, "", "");
    }

    @Override
    public void onLoadFinished(Loader<RestResponce> arg0, RestResponce responce) {
        if (responce.getStatusCode() == RestCall.httpStatusOK) {
            try {
                JSONObject object = new JSONObject(responce.getData());
                if (object.has("exist")) {
                    emailInUse.setVisibility(TextView.VISIBLE);
                } else {
                    notifier.popFragment();
                }
            } catch (JSONException e) {
            }
        } else {
            Toast.makeText(getActivity().getApplicationContext(), getResources().getString(R.string.error_no_connection), Toast.LENGTH_LONG).show();
        }
        getLoaderManager().destroyLoader(RestCall.NALOZI_REST);

    }

    @Override
    public void onLoaderReset(Loader<RestResponce> arg0) {
        // TODO Auto-generated method stub

    }

    private void callRest(String name, String surname, String email, String address, String city, String postcode, String country, String categoryId, String password) {
        // TODO move to strings
        String path = "http://www.city-go.eu/restful.php?type=register&";

        StringBuilder builder = new StringBuilder();
        builder.append(path);
        builder.append("ime=");
        builder.append(name);
        builder.append("&priimek=");
        builder.append(surname);
        builder.append("&email=");
        builder.append(email);
        builder.append("&naslov=");
        builder.append(address);
        builder.append("&mesto=");
        builder.append(city);
        builder.append("&postna_st=");
        builder.append(postcode);
        builder.append("&id_drzave=");
        builder.append(country);
        builder.append("&category_id=");
        builder.append(categoryId);
        builder.append("&geslo=");
        builder.append(password);
        builder.append("&hash=");
        builder.append(Values.KEY);

        Log.i("register", RestPath.createSpaceInPath(builder.toString()));

        Bundle args = new Bundle();
        args.putParcelable(RestCall.URI_REST, Uri.parse(RestPath.createSpaceInPath(builder.toString())));
        getLoaderManager().initLoader(RestCall.NALOZI_REST, args, this);
    }

    @Override
    public void onItemSelected(AdapterView<?> arg0, View view, int arg2, long arg3) {

        Long position = arg0.getItemIdAtPosition(arg2) + 1;
        country = position.toString();
    }

    @Override
    public void onNothingSelected(AdapterView<?> arg0) {
        // TODO Auto-generated method stub

    }

}
