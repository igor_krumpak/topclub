package si.iitech.topclub.fragments;

import java.util.List;

import org.json.JSONArray;


import si.iitech.library.fragment.IITechListFragment;
import si.iitech.library.rest.RestCall;
import si.iitech.library.rest.RestEnum;
import si.iitech.library.rest.RestResponce;
import si.iitech.topclub.R;
import si.iitech.topclub.adapter.MyCuponAdapter;
import si.iitech.topclub.common.RestPath;
import si.iitech.topclub.common.Values;
import si.iitech.topclub.object.Cupon;
import si.iitech.topclub.object.User;
import android.app.Activity;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.Loader;
import android.util.Log;
import android.view.View;
import android.widget.ListView;

public class MyCuponList extends IITechListFragment implements LoaderManager.LoaderCallbacks<RestResponce> {

	private ChangeFragment	notifier	= null;
	private List<Cupon>		list		= null;
	private double[]		from		= null;

	@Override
	public void onAttach(Activity activity) {
		super.onAttach(activity);
		try {
			notifier = (ChangeFragment) activity;
		} catch (ClassCastException e) {
		}
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
		getListView().setDivider(null);
		getListView().setDividerHeight(0);
		getListView().setBackgroundColor(getResources().getColor(R.color.gray));

		String userId = User.getUserID(getActivity());
		String rest = RestPath.createMyCuponPath(userId);
		from = (double[]) getArguments().getSerializable(Values.LOCATION_FROM);

		Log.d("my cupon path", rest);

		Bundle args = new Bundle();
		args.putParcelable(RestCall.URI_REST, Uri.parse(rest));
		getLoaderManager().initLoader(RestCall.NALOZI_REST, args, this);
	}

	@Override
	public Loader<RestResponce> onCreateLoader(int arg0, Bundle arg1) {
		Uri url = arg1.getParcelable(RestCall.URI_REST);
		return new RestCall(this.getActivity(), RestEnum.GET, url, "", "");
	}

	@Override
	public void onLoadFinished(Loader<RestResponce> arg0, RestResponce responce) {
		try {
			if (responce.getStatusCode() == RestCall.httpStatusOK) {
				String data = responce.getData();
				if (!responce.getData().equalsIgnoreCase("null")) {
					JSONArray array = new JSONArray(data);
					list = Cupon.parseJsonMyCupon(array);
					MyCuponAdapter adapter = new MyCuponAdapter(getActivity(), list, notifier);
					this.setListAdapter(adapter);
				} else {
					setEmptyText(getResources().getString(R.string.error_no_data));
					this.setListAdapter(null);
				}
			} else {
				setEmptyText(getResources().getString(R.string.error_no_connection));
				this.setListAdapter(null);
			}
		} catch (Exception e) {
			setEmptyText(getResources().getString(R.string.error));
			this.setListAdapter(null);
		}

	}

	@Override
	public void onLoaderReset(Loader<RestResponce> arg0) {
		// TODO Auto-generated method stub

	}

	/**
	 * holder.code.setOnClickListener(new OnClickListener() {
	 * 
	 * @Override public void onClick(View v) { Bundle bundle = new Bundle();
	 *           bundle.putSerializable(Values.CUPON, cupon);
	 *           notifier.changeFragment(TopClubFragment.CUPON_CODE, bundle,
	 *           false); } });
	 */

	@Override
	public void onListItemClick(ListView l, View v, int position, long id) {
		Cupon cupon = list.get(position);
		Bundle bundle = new Bundle();
		bundle.putSerializable(Values.LOCATION_FROM, from);
		bundle.putSerializable(Values.CUPON, cupon);
		notifier.changeFragment(TopClubFragment.CUPON_CODE, bundle, false);

	}

}
