package si.iitech.topclub.fragments;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.location.Location;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.Loader;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ListView;

import org.json.JSONArray;

import java.util.Collections;
import java.util.List;

import si.iitech.library.fragment.IITechListFragment;
import si.iitech.library.rest.RestCall;
import si.iitech.library.rest.RestEnum;
import si.iitech.library.rest.RestResponce;
import si.iitech.topclub.R;
import si.iitech.topclub.adapter.CuponAdapter;
import si.iitech.topclub.common.RestPath;
import si.iitech.topclub.common.Values;
import si.iitech.topclub.object.Cupon;

public class DiffrentCuponList extends IITechListFragment implements LoaderManager.LoaderCallbacks<RestResponce>, OnClickListener {

	private ChangeFragment	notifier		= null;
	private List<Cupon>		list			= null;
	private double[]		from			= null;
	private String regija = null;
	private String[]		realValues		= null;
	private String[]		displayValues	= null;

	@Override
	public void onAttach(Activity activity) {
		super.onAttach(activity);
		try {
			notifier = (ChangeFragment) activity;
		} catch (ClassCastException e) {
		}
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
		getListView().setDivider(null);
		getListView().setDividerHeight(0);
		getListView().setBackgroundColor(getResources().getColor(R.color.gray));
		setHasOptionsMenu(true);
		regija = getArguments().getString(Values.CATEGORY);
		from = (double[]) getArguments().getSerializable(Values.LOCATION_FROM);

		displayValues = getActivity().getResources().getStringArray(R.array.regije_array);
		realValues = getActivity().getResources().getStringArray(R.array.regije_values);

		String rest = RestPath.createCuponPath(regija, "0");
		Bundle args = new Bundle();
		Log.i("GET CUPONS", rest);
		args.putParcelable(RestCall.URI_REST, Uri.parse(rest));
		getLoaderManager().initLoader(RestCall.NALOZI_REST, args, this);
	}

	@Override
	public void onListItemClick(ListView l, View v, int position, long id) {
		Cupon cupon = list.get(position);
		Bundle bundle = new Bundle();
		bundle.putSerializable(Values.CUPON, cupon);
		bundle.putSerializable(Values.LOCATION_FROM, from);
		notifier.changeFragment(TopClubFragment.CUPON_DETAILS, bundle, false);
	}

	@Override
	public Loader<RestResponce> onCreateLoader(int arg0, Bundle arg1) {
		Uri url = arg1.getParcelable(RestCall.URI_REST);
		return new RestCall(this.getActivity(), RestEnum.GET, url, "", "");
	}

	@Override
	public void onLoadFinished(Loader<RestResponce> arg0, RestResponce responce) {
		try {
			if (responce.getStatusCode() == RestCall.httpStatusOK) {
				String data = responce.getData();
				if (!responce.getData().equalsIgnoreCase("null")) {
					JSONArray array = new JSONArray(data);
					list = Cupon.parseJsonCupons(array);
					if (from[0] != 0.0 && from[1] != 0.0) {
						sortByDistance(list);
					}
					CuponAdapter adapter = new CuponAdapter(getActivity().getApplicationContext(), list);
					updateListAdapter(adapter);

				} else {
					this.setListAdapter(null);
					setEmptyText(getResources().getString(R.string.error_no_data));
				}
			} else {
				this.setListAdapter(null);
				setEmptyText(getResources().getString(R.string.error_no_connection));
			}
		} catch (Exception e) {
			this.setListAdapter(null);
			setEmptyText(getResources().getString(R.string.error));
		}

	}

	protected void updateListAdapter(CuponAdapter adapter) {
		this.setListAdapter(adapter);
	}

	@Override
	public void onLoaderReset(Loader<RestResponce> arg0) {

	}

	private void sortByDistance(List<Cupon> list) {
		Location myLocation = new Location("");
		myLocation.setLatitude(from[0]);
		myLocation.setLongitude(from[1]);
		Location toLocation;
		for (Cupon cupon : list) {
			toLocation = new Location("");
			toLocation.setLatitude(cupon.getLat());
			toLocation.setLongitude(cupon.getLon());
			double distance = myLocation.distanceTo(toLocation);
			cupon.setDistance(distance);
		}
		Collections.sort(list);
	}

	@Override
	public void onDetach() {
		super.onDetach();
	}

	@Override
	public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
		inflater.inflate(R.menu.diffrent_cupon_list_menu, menu);
		super.onCreateOptionsMenu(menu, inflater);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case R.id.show_map:
			showMap();
			return true;
		case R.id.change_regija:
			showDialogOfCategories();
			return true;
		default:
			return super.onOptionsItemSelected(item);
		}
	}

	private void showDialogOfCategories() {
		AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
		builder.setTitle("Regije");
		builder.setItems(displayValues, this);
		builder.show();
	}

	private void showMap() {
		Bundle bundle = new Bundle();
		double to[] = new double[2];
		bundle.putSerializable(Values.LOCATION_FROM, from);
		bundle.putSerializable(Values.LOCATION_TO, to);
		bundle.putString(Values.CATEGORY, regija);
		bundle.putString(Values.DISCOUNT, "0");
		bundle.putString(Values.TYPE, Values.TYPE_CUPONS);
		notifier.changeFragment(TopClubFragment.MAP, bundle, false);
	}

	@Override
	public void onClick(DialogInterface dialog, int which) {
		regija = realValues[which];
		String rest = RestPath.createCuponPath(regija, "0");
		Bundle args = new Bundle();
		args.putParcelable(RestCall.URI_REST, Uri.parse(rest));
		getLoaderManager().restartLoader(RestCall.NALOZI_REST, args, this);

	}

}
