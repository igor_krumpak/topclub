package si.iitech.topclub.fragments;

import java.net.URLDecoder;
import java.util.Date;
import java.util.List;


import si.iitech.library.fragment.IITechFragment;
import si.iitech.library.rest.RestCall;
import si.iitech.library.rest.RestEnum;
import si.iitech.library.rest.RestResponce;
import si.iitech.topclub.R;
import si.iitech.topclub.VirtualCard;
import si.iitech.topclub.common.Arial;
import si.iitech.topclub.common.Formatter;
import si.iitech.topclub.common.RestPath;
import si.iitech.topclub.common.Values;
import si.iitech.topclub.object.Cupon;
import si.iitech.topclub.object.User;
import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.Loader;
import android.text.Html;
import android.util.Log;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.webkit.URLUtil;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ImageView.ScaleType;
import android.widget.LinearLayout;
import android.widget.LinearLayout.LayoutParams;
import android.widget.TextView;
import android.widget.Toast;

import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.assist.FailReason;
import com.nostra13.universalimageloader.core.listener.ImageLoadingListener;

public class CuponDetails extends IITechFragment implements LoaderManager.LoaderCallbacks<RestResponce>, OnClickListener {

	protected ChangeFragment	notifier				= null;

	private TextView			textViewTitle			= null;
	private TextView			textViewOldPrice		= null;
	private TextView			textViewSpecialPrice	= null;
	private TextView			textViewMetaData		= null;
	private ImageView			imageViewPicture		= null;
	private TextView			textViewLocation		= null;
	private TextView			textViewExpiration		= null;
	private Cupon				cupon					= null;
	private Button				buttonBuy				= null;
	private CountDownTimer		cuponExpirationTimer	= null;
	private double[]			from					= null;
	private LinearLayout		extraImagesHolder		= null;

	@Override
	public void onAttach(Activity activity) {
		super.onAttach(activity);
		try {
			notifier = (ChangeFragment) activity;
		} catch (ClassCastException e) {
		}
	}

	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		setHasOptionsMenu(true);
		View view = inflater.inflate(R.layout.cupon_details, container, false);
		textViewTitle = (TextView) view.findViewById(R.id.textView_cupon_title);
		textViewTitle.setTypeface(Arial.getInstance(getActivity()).getTypeFace());
		textViewOldPrice = (TextView) view.findViewById(R.id.textView_cupon_old_price);
		textViewOldPrice.setTypeface(Arial.getInstance(getActivity()).getTypeFace());
		textViewSpecialPrice = (TextView) view.findViewById(R.id.textView_cupon_special_price);
		textViewSpecialPrice.setTypeface(Arial.getInstance(getActivity()).getTypeFace());
		imageViewPicture = (ImageView) view.findViewById(R.id.imageView_cupon_picture);
		textViewExpiration = (TextView) view.findViewById(R.id.textView_cupon_expiration);
		textViewExpiration.setTypeface(Arial.getInstance(getActivity()).getTypeFace());
		textViewMetaData = (TextView) view.findViewById(R.id.cupon_details_metadata);
		textViewMetaData.setTypeface(Arial.getInstance(getActivity()).getTypeFace());
		textViewLocation = (TextView) view.findViewById(R.id.cupon_details_location);
		textViewLocation.setTypeface(Arial.getInstance(getActivity()).getTypeFace());
		buttonBuy = (Button) view.findViewById(R.id.button_cupon_buy);
		buttonBuy.setTypeface(Arial.getInstance(getActivity()).getTypeFace());
		buttonBuy.setOnClickListener(this);
		extraImagesHolder = (LinearLayout) view.findViewById(R.id.extra_images_holder);
		return view;
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);

		cupon = (Cupon) getArguments().get(Values.CUPON);

		textViewTitle.setText(cupon.getName());
		Formatter.formatCuponOutput(getActivity().getApplicationContext(), textViewOldPrice, textViewSpecialPrice, cupon, null,
				getResources().getString(R.string.procent_text) + " ");
		String rulesOfTheCuponGame = cupon.getRulesOfTheCuponGame();
		Log.i("rules", rulesOfTheCuponGame);
		// rulesOfTheCuponGame =
		// rulesOfTheCuponGame.replaceAll("\\\\r\\\\n\\\\r\\\\n",
		// System.getProperty("line.separator"));

		// String test = StringEscapeUtils.unescapeJava(rulesOfTheCuponGame);
		textViewMetaData.setText(Html.fromHtml(cupon.getRulesOfTheCuponGame()));
		textViewLocation.setText(cupon.getLocation());
		calculateExpiration(cupon.getEndTime());

		Log.i("description", cupon.getDescription());
		from = (double[]) getArguments().getSerializable(Values.LOCATION_FROM);

		ImageLoader.getInstance().displayImage(cupon.getPicture(), imageViewPicture);

		List<String> extraImages = cupon.getExtraImages();
		creteExtraImagesHolder(cupon.getDescription(), extraImages);
		notifier.startComercial();
	}

	private void creteExtraImagesHolder(String text, List<String> extraImages) {

		text = text.replace("{", "").replace("}", "");
		String[] sections = text.split("image");

		for (int i = 0; i < sections.length; i++) {
			String section = sections[i];
			TextView textView = new TextView(getActivity());
			textView.setBackgroundColor(getResources().getColor(R.color.white));
			LayoutParams params = new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT);
			textView.setLayoutParams(params);
			textView.setTypeface(Arial.getInstance(getActivity()).getTypeFace());
			textView.setText(Html.fromHtml(section));
			extraImagesHolder.addView(textView);

			if (i < extraImages.size()) {
				String image = extraImages.get(i);
				if (!image.contains("www.city-go.eu")) {
					image = "http://www.city-go.eu" + image;
					Log.i("contains", "true");
				}

				final ImageView extraImageView = new ImageView(getActivity());
				int height = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 200, getResources().getDisplayMetrics());
				params = new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT);
				extraImageView.setLayoutParams(params);
				extraImageView.setBackgroundColor(getResources().getColor(R.color.white));
				extraImageView.setScaleType(ScaleType.CENTER_CROP);
				extraImagesHolder.addView(extraImageView);

				// TODO

				try {
					String url = URLDecoder.decode(image, "UTF-8");
					Log.i("decode", url);
					if (URLUtil.isValidUrl(url))
						ImageLoader.getInstance().displayImage(url, extraImageView, new ImageLoadingListener() {

							@Override
							public void onLoadingStarted(String arg0, View arg1) {
								// TODO Auto-generated method stub

							}

							@Override
							public void onLoadingFailed(String arg0, View arg1, FailReason arg2) {
								extraImageView.setVisibility(View.GONE);

							}

							@Override
							public void onLoadingComplete(String arg0, View arg1, Bitmap arg2) {
								// TODO Auto-generated method stub

							}

							@Override
							public void onLoadingCancelled(String arg0, View arg1) {
								extraImageView.setVisibility(View.GONE);

							}
						});
					else
						extraImageView.setVisibility(View.GONE);
				} catch (Exception e) {
					extraImageView.setVisibility(View.GONE);
				}
			}
		}
	}

	private void calculateExpiration(long endTime) {
		long milliseconds = endTime - (new Date()).getTime();
		cuponExpirationTimer = new CountDownTimer(milliseconds, 1000) {
			@Override
			public void onTick(long millisUntilFinished) {
				String cuponExpiration = null;
				int seconds = (int) (millisUntilFinished / 1000) % 60;
				int minutes = (int) ((millisUntilFinished / (1000 * 60)) % 60);
				int hours = (int) ((millisUntilFinished / (1000 * 60 * 60)) % 24);
				int days = (int) ((millisUntilFinished / (1000 * 60 * 60 * 24)));

				String dayText = "";
				String hourText = "u";
				String minText = "m";
				String secText = "s";

				if (days == 1) {
					dayText = "Dan";
				} else if (days == 2) {
					dayText = "Dneva";
				} else if (days > 2 && days < 5) {
					dayText = "Dnevi";
				} else {
					dayText = "Dni";
				}

				cuponExpiration = days + " " + dayText + "  " + hours + hourText + " " + minutes + minText + " " + seconds + secText;
				textViewExpiration.setText(cuponExpiration);
			}

			@Override
			public void onFinish() {

			}
		}.start();
	}

	@Override
	protected void initGraphicComponents() {
	}

	@Override
	public Loader<RestResponce> onCreateLoader(int arg0, Bundle arg1) {
		Uri url = arg1.getParcelable(RestCall.URI_REST);
		return new RestCall(this.getActivity(), RestEnum.GET, url, "", "");
	}

	@Override
	public void onLoadFinished(Loader<RestResponce> arg0, RestResponce responce) {
		if (responce.getStatusCode() == RestCall.httpStatusOK) {

		} else {
			Toast.makeText(getActivity().getApplicationContext(), getResources().getString(R.string.error_cupon_bought), Toast.LENGTH_LONG).show();
		}

	}

	@Override
	public void onLoaderReset(Loader<RestResponce> arg0) {

	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.button_cupon_buy:
			if (User.userExists(getActivity())) {
				Intent intent = new Intent(getActivity(), VirtualCard.class);
				startActivity(intent);
			} else {
				notifier.changeFragment(TopClubFragment.LOGIN, null, false);
			}
			break;
		case R.id.cupon_details_button_navigation:
			openNavigation();
			break;
		}

	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case R.id.start_navigation:
			openNavigation();
			return true;
		default:
			return super.onOptionsItemSelected(item);
		}
	}

	private void openNavigation() {
		if (cupon.getLat() != 0.0 && cupon.getLon() != 0.0) {
			String navPath = "http://maps.google.com/maps?";
			StringBuilder sb = new StringBuilder();
			sb.append(navPath);
			sb.append("saddr=");
			sb.append(from[0] + "," + from[1]);
			sb.append("&daddr=");
			sb.append(cupon.getLat());
			sb.append(",");
			sb.append(cupon.getLon());
			Intent navigation = new Intent(Intent.ACTION_VIEW, Uri.parse(sb.toString()));
			startActivity(navigation);
		} else {
			Toast.makeText(getActivity().getApplicationContext(), getResources().getString(R.string.map_no_location), Toast.LENGTH_LONG).show();
		}

	}

	@Override
	public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
		menu.removeItem(R.id.change_category);
		menu.removeItem(R.id.show_map);
		inflater.inflate(R.menu.cupon_details_menu, menu);
		super.onCreateOptionsMenu(menu, inflater);
	}
}
