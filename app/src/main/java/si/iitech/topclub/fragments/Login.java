package si.iitech.topclub.fragments;


import si.iitech.library.fragment.IITechFragment;
import si.iitech.library.rest.RestCall;
import si.iitech.library.rest.RestEnum;
import si.iitech.library.rest.RestResponce;
import si.iitech.library.util.IITechUtil;
import si.iitech.topclub.R;
import si.iitech.topclub.common.Arial;
import si.iitech.topclub.common.RestPath;
import si.iitech.topclub.common.Values;
import si.iitech.topclub.object.User;

import android.app.Activity;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.LoaderManager.LoaderCallbacks;
import android.support.v4.content.Loader;
import android.text.method.LinkMovementMethod;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class Login extends IITechFragment implements LoaderCallbacks<RestResponce>, OnClickListener {

	private EditText		editTextEmail		= null;
	private EditText		editTextPassword	= null;
	private Button			buttonLogin			= null;
	private TextView		loginWarning		= null;
	private TextView		forgottenPassword	= null;
	private TextView		loginRegistration	= null;
	private ChangeFragment	notifier;

	public Login() {
	}

	@Override
	public void onAttach(Activity activity) {
		super.onAttach(activity);
		try {
			notifier = (ChangeFragment) activity;
		} catch (ClassCastException e) {
		}
	}

	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

		View view = inflater.inflate(R.layout.login, container, false);

		editTextEmail = (EditText) view.findViewById(R.id.email);
		editTextEmail.setTypeface(Arial.getInstance(getActivity()).getTypeFace());
		editTextPassword = (EditText) view.findViewById(R.id.password);
		editTextPassword.setTypeface(Arial.getInstance(getActivity()).getTypeFace());
		buttonLogin = (Button) view.findViewById(R.id.login);
		buttonLogin.setTypeface(Arial.getInstance(getActivity()).getTypeFace());
		buttonLogin.setOnClickListener(this);
		loginWarning = (TextView) view.findViewById(R.id.login_warning);
		loginWarning.setTypeface(Arial.getInstance(getActivity()).getTypeFace());
		loginWarning.setVisibility(View.INVISIBLE);
		forgottenPassword = (TextView) view.findViewById(R.id.textView_forgotten_pass);
		forgottenPassword.setTypeface(Arial.getInstance(getActivity()).getTypeFace());
		forgottenPassword.setMovementMethod(LinkMovementMethod.getInstance());
		loginRegistration = (TextView) view.findViewById(R.id.login_registration);
		loginRegistration.setTypeface(Arial.getInstance(getActivity()).getTypeFace());
		loginRegistration.setOnClickListener(this);

		return view;
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
	}

	@Override
	public Loader<RestResponce> onCreateLoader(int arg0, Bundle args) {
		Uri url = args.getParcelable(RestCall.URI_REST);
		return new RestCall(this.getActivity(), RestEnum.GET, url, "", "");
	}

	@Override
	public void onLoadFinished(Loader<RestResponce> arg0, RestResponce responce) {
		if (responce.getStatusCode() == RestCall.httpStatusOK) {
			if (User.login(getActivity(), responce.getData())) {
				notifier.popFragment();
			} else {
				loginWarning.setVisibility(View.VISIBLE);
			}
		} else {
			Toast.makeText(getActivity().getApplicationContext(), getResources().getString(R.string.error_no_connection), Toast.LENGTH_LONG).show();
		}
		getLoaderManager().destroyLoader(RestCall.NALOZI_REST);
	}

	@Override
	public void onLoaderReset(Loader<RestResponce> arg0) {

	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.login:
			String email = editTextEmail.getText().toString();
			String password = editTextPassword.getText().toString();
			IITechUtil.hideKeyboard(getActivity());
			callRest(email, password);
			break;
		case R.id.login_registration:
			notifier.changeFragment(TopClubFragment.REGISTRATION, null, false);
			break;
		}
	}

	private void callRest(String email, String password) {

		if (IITechUtil.checkValues(editTextEmail, Values.EMAIL_PATTERN, getResources().getString(R.string.validation_email),
				getResources().getString(R.string.validation_wrong_email))
				&& IITechUtil.checkValues(editTextPassword, null, getResources().getString(R.string.validation_password), null)) {
			String path = "http://www.city-go.eu/restful.php?type=login&";
			StringBuilder builder = new StringBuilder();
			builder.append(path);
			builder.append("email=");
			builder.append(email);
			builder.append("&pass=");
			builder.append(password);
			builder.append("&hash=");
			builder.append(Values.KEY);

			Bundle args = new Bundle();
			Log.i("login", builder.toString());
			args.putParcelable(RestCall.URI_REST, Uri.parse(RestPath.makeStringUrlFriendly(builder.toString())));
			getLoaderManager().initLoader(RestCall.NALOZI_REST, args, this);
		}
	}

	@Override
	protected void initGraphicComponents() {

	}
}
