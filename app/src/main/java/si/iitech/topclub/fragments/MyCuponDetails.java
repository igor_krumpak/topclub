package si.iitech.topclub.fragments;


import si.iitech.library.fragment.IITechFragment;
import si.iitech.library.rest.RestCall;
import si.iitech.library.rest.RestEnum;
import si.iitech.library.rest.RestResponce;
import si.iitech.topclub.R;
import si.iitech.topclub.common.Arial;
import si.iitech.topclub.common.Formatter;
import si.iitech.topclub.common.RestPath;
import si.iitech.topclub.common.Values;
import si.iitech.topclub.object.Cupon;
import si.iitech.topclub.object.User;
import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.Loader;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.nostra13.universalimageloader.core.ImageLoader;

public class MyCuponDetails extends IITechFragment implements OnClickListener, LoaderManager.LoaderCallbacks<RestResponce> {

	protected ChangeFragment	notifier		= null;
	private ImageView			imageViewCode	= null;
	private TextView			name			= null;
	private TextView			price			= null;
	private TextView			payPrice		= null;
	private TextView			cuponId			= null;
	private TextView			cuponOwner		= null;
	private TextView			expirationDate	= null;
	private TextView			metaData		= null;
	private TextView			address			= null;
	private Button				deleteButton	= null;
	private Cupon				cupon			= null;
	private double[]			from			= null;

	@Override
	public void onAttach(Activity activity) {
		super.onAttach(activity);
		try {
			notifier = (ChangeFragment) activity;
		} catch (ClassCastException e) {
		}
	}

	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.my_cupon_details, container, false);
		imageViewCode = (ImageView) view.findViewById(R.id.imageView_code);
		name = (TextView) view.findViewById(R.id.my_cupon_details_name);
		name.setTypeface(Arial.getInstance(getActivity()).getTypeFace());
		price = (TextView) view.findViewById(R.id.my_cupon_details_cupon_value);
		price.setTypeface(Arial.getInstance(getActivity()).getTypeFace());
		payPrice = (TextView) view.findViewById(R.id.my_cupon_details_special_price);
		payPrice.setTypeface(Arial.getInstance(getActivity()).getTypeFace());
		cuponId = (TextView) view.findViewById(R.id.my_cupon_details_cupon_id);
		cuponId.setTypeface(Arial.getInstance(getActivity()).getTypeFace());
		cuponOwner = (TextView) view.findViewById(R.id.my_cupon_details_owner);
		cuponOwner.setTypeface(Arial.getInstance(getActivity()).getTypeFace());
		expirationDate = (TextView) view.findViewById(R.id.my_cupon_details_expiration_time);
		expirationDate.setTypeface(Arial.getInstance(getActivity()).getTypeFace());
		metaData = (TextView) view.findViewById(R.id.my_cupon_details_meta_description);
		metaData.setTypeface(Arial.getInstance(getActivity()).getTypeFace());
		address = (TextView) view.findViewById(R.id.my_cupon_details_address);
		address.setTypeface(Arial.getInstance(getActivity()).getTypeFace());
		deleteButton = (Button) view.findViewById(R.id.button_delete_my_cupon);
		deleteButton.setTypeface(Arial.getInstance(getActivity()).getTypeFace());
		deleteButton.setOnClickListener(this);
		return view;
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
		setHasOptionsMenu(true);
		from = (double[]) getArguments().getSerializable(Values.LOCATION_FROM);
		cupon = (Cupon) getArguments().getSerializable(Values.CUPON);
		cuponId.setText(cupon.getId() + "");
		name.setText(cupon.getName());
		price.setText(cupon.getPrice() + " " + Values.EURO_SIGN + ",");
		Formatter.formatCuponOutput(getActivity().getApplicationContext(), price, payPrice, cupon, null, getResources().getString(R.string.procent_text) + " ");
		cuponOwner.setText(User.getUserNameAndSurname(getActivity()));
		expirationDate.setText(cupon.getEndDate());
		metaData.setText(Html.fromHtml(cupon.getRulesOfTheCuponGame()));
		address.setText(cupon.getLocation());
		// NAstavi ostale stvari
		ImageLoader.getInstance().displayImage(RestPath.createCuponCodePath(50, cupon.getId()), imageViewCode);
	}

	@Override
	protected void initGraphicComponents() {

	}

	@Override
	public void onClick(View v) {
		if (v.getId() == R.id.button_delete_my_cupon) {
			startRestDelete();
		}

	}

	private void startRestDelete() {
		if (cupon != null) {
			String rest = RestPath.createDeleteMyCuponPath(cupon.getDeleteId());
			Bundle args = new Bundle();
			args.putParcelable(RestCall.URI_REST, Uri.parse(rest));
			getLoaderManager().initLoader(RestCall.NALOZI_REST, args, this);
		}
	}

	@Override
	public Loader<RestResponce> onCreateLoader(int arg0, Bundle arg1) {
		Uri url = arg1.getParcelable(RestCall.URI_REST);
		return new RestCall(this.getActivity(), RestEnum.GET, url, "", "");
	}

	@Override
	public void onLoadFinished(Loader<RestResponce> arg0, RestResponce responce) {
		if (responce.getStatusCode() == RestCall.httpStatusOK) {
			notifier.popFragment();
		} else {
			Toast.makeText(getActivity().getApplicationContext(), getResources().getString(R.string.error_no_connection), Toast.LENGTH_LONG).show();
		}
	}

	@Override
	public void onLoaderReset(Loader<RestResponce> arg0) {
		// TODO Auto-generated method stub
	}

	@Override
	public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
		inflater.inflate(R.menu.cupon_details_menu, menu);
		super.onCreateOptionsMenu(menu, inflater);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case R.id.start_navigation:
			openNavigation();
			return true;
		default:
			return super.onOptionsItemSelected(item);
		}
	}

	private void openNavigation() {
		if (cupon.getLat() != 0.0 && cupon.getLon() != 0.0) {
			String navPath = "http://maps.google.com/maps?";
			StringBuilder sb = new StringBuilder();
			sb.append(navPath);
			sb.append("saddr=");
			sb.append(from[0] + "," + from[1]);
			sb.append("&daddr=");
			sb.append(cupon.getLat());
			sb.append(",");
			sb.append(cupon.getLon());
			Intent navigation = new Intent(Intent.ACTION_VIEW, Uri.parse(sb.toString()));
			startActivity(navigation);
		} else {
			Toast.makeText(getActivity().getApplicationContext(), getResources().getString(R.string.map_no_location), Toast.LENGTH_LONG).show();
		}

	}

}
