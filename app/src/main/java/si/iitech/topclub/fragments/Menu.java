package si.iitech.topclub.fragments;


import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import si.iitech.library.fragment.IITechFragment;
import si.iitech.topclub.R;
import si.iitech.topclub.VirtualCard;
import si.iitech.topclub.common.Arial;
import si.iitech.topclub.common.Values;
import si.iitech.topclub.object.User;

public class Menu extends IITechFragment implements LocationListener {

    private TextView loggedInAs = null;
    private TextView logout = null;
    private Button couponsNearby = null;
    private Button couponsOnWay = null;
    private Button topDiscounts = null;
    private Button topCupons = null;
    private Button news = null;
    private Button attractions = null;
    private Button barCodes = null;
    private Button ekos = null;
    private ChangeFragment notifier = null;
    private ProgressDialog progressDialog = null;
    private LocationManager locationManager = null;
    private int selectedCategory;
    private int selectedDiscount;
    private TopClubFragment nextFragment;

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            notifier = (ChangeFragment) activity;
        } catch (ClassCastException e) {
        }
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.menu, container, false);
        logout = (TextView) view.findViewById(R.id.textView_logout);
        logout.setTypeface(Arial.getInstance(getActivity()).getTypeFace());
        logout.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                User.logut(getActivity());
                loggedInAs.setText(User.getUserNameAndSurname(getActivity()));
                onResume();
            }
        });

        loggedInAs = (TextView) view.findViewById(R.id.textView_logged_in_as);
        loggedInAs.setTypeface(Arial.getInstance(getActivity()).getTypeFace());

        couponsNearby = (Button) view.findViewById(R.id.button_discounts_nearby);
        couponsNearby.setTypeface(Arial.getInstance(getActivity()).getTypeFace());
        couponsNearby.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                selectedCategory = 0;
                selectedDiscount = 0;
                nextFragment = TopClubFragment.CUPON_LIST;
                startLocationListner();

            }
        });

        topCupons = (Button) view.findViewById(R.id.top_cupons);
        topCupons.setTypeface(Arial.getInstance(getActivity()).getTypeFace());
        topCupons.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                selectedCategory = 59;
                selectedDiscount = 0;
                nextFragment = TopClubFragment.DIFFRENT_CUPON_LIST;
                startLocationListner();

            }
        });

        couponsOnWay = (Button) view.findViewById(R.id.button_discounts_on_the_way);
        couponsOnWay.setTypeface(Arial.getInstance(getActivity()).getTypeFace());
        couponsOnWay.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                notifier.changeFragment(TopClubFragment.CUPON_FILTER_ON_THE_WAY, null, false);

            }
        });

        topDiscounts = (Button) view.findViewById(R.id.button_top_discounts);
        topDiscounts.setTypeface(Arial.getInstance(getActivity()).getTypeFace());
        topDiscounts.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                if (User.userExists(getActivity())) {
                    Intent intent = new Intent(getActivity(), VirtualCard.class);
                    startActivity(intent);
                } else {
                    notifier.changeFragment(TopClubFragment.LOGIN, null, false);
                }
            }
        });

        news = (Button) view.findViewById(R.id.button_news);
        news.setTypeface(Arial.getInstance(getActivity()).getTypeFace());
        news.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
               notifier.changeFragment(TopClubFragment.NEWS_LIST, null, false);
            }
        });

        attractions = (Button) view.findViewById(R.id.button_attractions);
        attractions.setTypeface(Arial.getInstance(getActivity()).getTypeFace());
        attractions.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                /**
                 * Bundle bundle = new Bundle();
                 bundle.putSerializable(Values.LOCATION_FROM, new double[]{0.0, 0.0});
                 notifier.changeFragment(TopClubFragment.ATTRACTION_LIST, bundle, false);
                 */
                nextFragment = TopClubFragment.ATTRACTION_LIST;
                startLocationListner();
            }
        });

        barCodes = (Button) view.findViewById(R.id.button_barcodes);
        barCodes.setTypeface(Arial.getInstance(getActivity()).getTypeFace());
        barCodes.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                notifier.changeFragment(TopClubFragment.BAR_CODE_LIST, null, false);
            }
        });

        ekos = (Button) view.findViewById(R.id.button_eko);
        ekos.setTypeface(Arial.getInstance(getActivity()).getTypeFace());
        ekos.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {

                /**
                Bundle bundle = new Bundle();
                bundle.putSerializable(Values.LOCATION_FROM, new double[]{0.0, 0.0});
                notifier.changeFragment(TopClubFragment.EKO_LIST, bundle, false);
                 *
                 */
                nextFragment = TopClubFragment.EKO_LIST;
                startLocationListner();
            }
        });
        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        if (User.userExists(getActivity())) {
            loggedInAs.setText(User.getUserNameAndSurname(getActivity()));
            logout.setVisibility(View.VISIBLE);
        } else {
            logout.setVisibility(View.INVISIBLE);
        }

    }

    private void startLocationListner() {
        try {
            locationManager = (LocationManager) getActivity().getSystemService(Context.LOCATION_SERVICE);
            progressDialog = new ProgressDialog(getActivity());
            progressDialog.setMessage(getString(R.string.location_fetching));
            progressDialog.setIndeterminate(true);
            try {
                progressDialog.show();
            } catch (Exception e) {
            }

            locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 0, 0, this);
            locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 0, this);

        } catch (Exception e) {

        }
    }

    private void startTopRegije(Location location) {
        try {
            Bundle bundle = new Bundle();
            bundle.putSerializable(Values.LOCATION_FROM, new double[]{location.getLatitude(), location.getLongitude()});
            bundle.putSerializable(Values.LOCATION_TO, new double[2]);
            bundle.putString(Values.CATEGORY, String.valueOf(selectedCategory));
            notifier.changeFragment(TopClubFragment.DIFFRENT_CUPON_LIST, bundle, false);

        } catch (Exception e) {

        }
    }

    private void startTopDiscounts(Location location) {
        try {
            Bundle bundle = new Bundle();
            bundle.putSerializable(Values.LOCATION_FROM, new double[]{location.getLatitude(), location.getLongitude()});
            bundle.putSerializable(Values.LOCATION_TO, new double[2]);
            bundle.putString(Values.CATEGORY, String.valueOf(selectedCategory));
            bundle.putString(Values.DISCOUNT, String.valueOf(selectedDiscount));
            notifier.changeFragment(TopClubFragment.CUPON_LIST, bundle, false);

        } catch (Exception e) {

        }
    }

    @Override
    protected void initGraphicComponents() {

    }

    @Override
    public void onLocationChanged(Location location) {
        try {
            locationManager.removeUpdates(this);
            progressDialog.hide();
            if (nextFragment == TopClubFragment.CUPON_LIST) {
                startTopDiscounts(location);
            } else if (nextFragment == TopClubFragment.MY_CUPONS) {
                startMyCupon(location);
            } else if(nextFragment == TopClubFragment.DIFFRENT_CUPON_LIST) {
                startTopRegije(location);
            } else if (nextFragment == TopClubFragment.ATTRACTION_LIST) {
                startAttractions(location);
            } else if(nextFragment == TopClubFragment.EKO_LIST) {
                startEko(location);
            }
        } catch (Exception e) {

        }
    }

    private void startAttractions(Location location) {
        try {
            Bundle bundle = new Bundle();
            bundle.putSerializable(Values.LOCATION_FROM, new double[]{location.getLatitude(), location.getLongitude()});
            notifier.changeFragment(TopClubFragment.ATTRACTION_LIST, bundle, false);
        } catch (Exception e) {

        }
    }

    private void startEko(Location location) {
        try {
            Bundle bundle = new Bundle();
            bundle.putSerializable(Values.LOCATION_FROM, new double[]{location.getLatitude(), location.getLongitude()});
            notifier.changeFragment(TopClubFragment.EKO_LIST, bundle, false);
        } catch (Exception e) {

        }
    }

    private void startMyCupon(Location location) {
        try {
            Bundle bundle = new Bundle();
            bundle.putSerializable(Values.LOCATION_FROM, new double[]{location.getLatitude(), location.getLongitude()});
            bundle.putSerializable(Values.LOCATION_TO, new double[2]);
            notifier.changeFragment(TopClubFragment.MY_CUPONS, bundle, false);
        } catch (Exception e) {

        }
    }



    @Override
    public void onProviderDisabled(String provider) {
        try {
            Location location = locationManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
            if (location != null) {
                locationManager.removeUpdates(this);
                progressDialog.hide();

                if (nextFragment == TopClubFragment.CUPON_LIST) {
                    startTopDiscounts(location);
                } else if (nextFragment == TopClubFragment.MY_CUPONS) {
                    startMyCupon(location);
                }

            } else {
                progressDialog.hide();
                Toast.makeText(getActivity(), getResources().getString(R.string.error_no_location), Toast.LENGTH_LONG).show();
            }
        } catch (Exception e) {

        }

    }

    @Override
    public void onProviderEnabled(String provider) {
        // TODO Auto-generated method stub

    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {
        // TODO Auto-generated method stub

    }

}
