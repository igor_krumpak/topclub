package si.iitech.topclub.fragments;

import si.iitech.topclub.R;
import android.location.Location;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;

public class CuponFilterNearby extends CuponFilterOnTheWay {

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		View view = super.onCreateView(inflater, container, savedInstanceState);
		textViewFrom.setText(getResources().getString(R.string.text_view_from_not_to));
		textViewTo.setVisibility(TextView.GONE);
		editTextAddressTo.setVisibility(EditText.GONE);
		return view;
	}

	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.cupon_filter_button_submit:
			int id = radioGroupAddress.getCheckedRadioButtonId();
			View radioButton = radioGroupAddress.findViewById(id);
			if (radioButton.getId() == R.id.cupon_filter_radio_button_position) {
				requestLocationUpdates();
			} else {
				double[] from = checkLocation(editTextAddressFrom);
				double[] to = new double[2];
				if (from != null) {
					displayResults(from, to);
					break;
				}
			}
		}
		super.onClick(v);
	}

	protected void displayResults(Location location) {
		double from[] = new double[] { location.getLatitude(), location.getLongitude() };
		double to[] = new double[2];
		displayResults(from, to);
	}

}
