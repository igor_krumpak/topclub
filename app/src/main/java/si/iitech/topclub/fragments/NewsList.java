package si.iitech.topclub.fragments;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.Loader;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Spinner;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.List;

import si.iitech.library.fragment.IITechListFragment;
import si.iitech.library.rest.RestCall;
import si.iitech.library.rest.RestEnum;
import si.iitech.library.rest.RestResponce;
import si.iitech.topclub.R;
import si.iitech.topclub.adapter.NewsAdapter;
import si.iitech.topclub.common.RestPath;
import si.iitech.topclub.common.Values;
import si.iitech.topclub.object.News;

public class NewsList extends IITechListFragment implements LoaderManager.LoaderCallbacks<RestResponce>, DialogInterface.OnClickListener {

    private ChangeFragment notifier = null;
    private List<News> list = null;
    private AlertDialog obcineDialog;

    private List<String> obcineId;
    private String latestObcineId;
    private String OBCINE_TAG = "OBCINE_TAG";

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            notifier = (ChangeFragment) activity;
        } catch (ClassCastException e) {
        }
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        getListView().setDivider(null);
        getListView().setDividerHeight(0);
        getListView().setBackgroundColor(getResources().getColor(R.color.gray));
        setHasOptionsMenu(true);

        AlertDialog.Builder builder  = new AlertDialog.Builder(getActivity());
        final List<String> obcineName = populateObcineName();
        obcineId = populateObcineId();

        ArrayAdapter<String> adapterObcine = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_dropdown_item, obcineName);
        adapterObcine.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        builder.setTitle(R.string.obcine);
        builder.setAdapter(adapterObcine, this);
        obcineDialog = builder.create();

        String savedObcineId = retriveFromSharedPreferences(OBCINE_TAG, "");
        if(savedObcineId.isEmpty()) {
            obcineDialog.show();
        } else {
            startObcineRest(savedObcineId);
        }

    }

    private List<String> populateObcineName() {
        List<String> list = null;
        InputStream jsonStream = this.getResources().openRawResource(R.raw.obcine);
        try {
            JSONArray jsonObcine = new JSONArray(convertStreamToString(jsonStream));
            list = new ArrayList<String>();
            for (int i = 0; i < jsonObcine.length(); i++) {
                JSONObject jsonCountry = jsonObcine.getJSONObject(i);
                list.add(jsonCountry.getString("name"));
            }
            return list;
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return list;
    }

    private String convertStreamToString(InputStream is) {
        ByteArrayOutputStream oas = new ByteArrayOutputStream();
        copyStream(is, oas);
        String t = oas.toString();
        try {
            oas.close();
            oas = null;
        } catch (IOException e) {
            e.printStackTrace();
        }
        return t;
    }

    private void copyStream(InputStream is, OutputStream os) {
        final int buffer_size = 1024;
        try {
            byte[] bytes = new byte[buffer_size];
            for (; ; ) {
                int count = is.read(bytes, 0, buffer_size);
                if (count == -1)
                    break;
                os.write(bytes, 0, count);
            }
        } catch (Exception ex) {
        }
    }

    private List<String> populateObcineId() {
        List<String> list = null;
        InputStream jsonStream = this.getResources().openRawResource(R.raw.obcine);
        try {
            JSONArray jsonObcine = new JSONArray(convertStreamToString(jsonStream));
            list = new ArrayList<String>();
            for (int i = 0; i < jsonObcine.length(); i++) {
                JSONObject jsonCountry = jsonObcine.getJSONObject(i);
                list.add(jsonCountry.getString("category_id"));
            }
            return list;
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return list;
    }

    @Override
    public void onListItemClick(ListView l, View v, int position, long id) {
        News news = list.get(position);
        Bundle bundle = new Bundle();
        bundle.putSerializable(Values.NEWS, news);
        notifier.changeFragment(TopClubFragment.NEWS_DETAILS, bundle, false);
    }

    @Override
    public Loader<RestResponce> onCreateLoader(int arg0, Bundle arg1) {
        Uri url = arg1.getParcelable(RestCall.URI_REST);
        return new RestCall(this.getActivity(), RestEnum.GET, url, "", "");
    }

    @Override
    public void onLoadFinished(Loader<RestResponce> arg0, RestResponce responce) {
        try {
            if (responce.getStatusCode() == RestCall.httpStatusOK) {
                String data = responce.getData();
                if (!responce.getData().equalsIgnoreCase("null")) {
                    JSONArray array = new JSONArray(data);
                    list = News.parseJsonNews(array);
                    NewsAdapter adapter = new NewsAdapter(getActivity().getApplicationContext(), list);
                    updateListAdapter(adapter);

                } else {
                    this.setListAdapter(null);
                    setEmptyText(getResources().getString(R.string.error_no_data));
                }
            } else {
                this.setListAdapter(null);
                setEmptyText(getResources().getString(R.string.error_no_connection));
            }
        } catch (Exception e) {
            this.setListAdapter(null);
            setEmptyText(getResources().getString(R.string.error));
        }
    }

    protected void updateListAdapter(NewsAdapter adapter) {
        this.setListAdapter(adapter);
    }

    @Override
    public void onLoaderReset(Loader<RestResponce> arg0) {

    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.news_list_menu, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.change_obcino:
                obcineDialog.show();
                return true;
            case R.id.save_obcino:
                if(latestObcineId != null && !latestObcineId.isEmpty())
                    saveInSharedPreferences(OBCINE_TAG, latestObcineId);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }


    @Override
    public void onClick(DialogInterface dialogInterface, int index) {
        latestObcineId = obcineId.get(index);
        startObcineRest(latestObcineId);
    }

    private void startObcineRest(String obcineId) {
        String rest = RestPath.createNewsPath(obcineId);
        Bundle args = new Bundle();
        Log.i("GET NEWS", rest);
        args.putParcelable(RestCall.URI_REST, Uri.parse(rest));
        getLoaderManager().restartLoader(RestCall.NALOZI_REST, args, this);
    }
}
