package si.iitech.topclub.fragments;

import android.os.Bundle;

public interface ChangeFragment {
	
	public void changeFragment(TopClubFragment fragment, Bundle bundle, boolean remove);

	public void popFragment();

	public void startComercial();
	
}
