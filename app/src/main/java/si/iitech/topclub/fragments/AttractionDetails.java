package si.iitech.topclub.fragments;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.text.Html;
import android.text.method.LinkMovementMethod;
import android.util.Log;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.webkit.URLUtil;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.assist.FailReason;
import com.nostra13.universalimageloader.core.listener.ImageLoadingListener;

import java.net.URLDecoder;
import java.util.List;

import si.iitech.library.fragment.IITechFragment;
import si.iitech.topclub.R;
import si.iitech.topclub.common.Arial;
import si.iitech.topclub.common.Values;
import si.iitech.topclub.object.Attraction;
import si.iitech.topclub.object.Cupon;

public class AttractionDetails extends IITechFragment {

    protected ChangeFragment notifier = null;

    private TextView textViewTitle = null;
    private ImageView imageViewPicture = null;
    private TextView textViewDescriptionTitle = null;
    private LinearLayout extraImagesHolder = null;

    private Attraction attraction = null;
    private double[] from = null;

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            notifier = (ChangeFragment) activity;
        } catch (ClassCastException e) {
        }
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.attraction_details, container, false);
        textViewTitle = (TextView) view.findViewById(R.id.textView_attraction_title);
        textViewTitle.setTypeface(Arial.getInstance(getActivity()).getTypeFace());
        imageViewPicture = (ImageView) view.findViewById(R.id.imageView_attraction_picture);
        textViewDescriptionTitle = (TextView)view.findViewById(R.id.textView_description_title);
        textViewDescriptionTitle.setTypeface(Arial.getInstance(getActivity()).getTypeFace());
        extraImagesHolder = (LinearLayout) view.findViewById(R.id.extra_images_holder);
        return view;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        setHasOptionsMenu(true);
        attraction = (Attraction) getArguments().get(Values.ATTRACTION);
        textViewTitle.setText(attraction.getTitle());
        from = (double[]) getArguments().getSerializable(Values.LOCATION_FROM);
        ImageLoader.getInstance().displayImage(attraction.getPicture(), imageViewPicture);

        if(attraction.getContent().isEmpty() && attraction.getDescription().isEmpty()) {
            textViewDescriptionTitle.setVisibility(View.GONE);
        } else {
            creteExtraImagesHolder(attraction.getDescription() + "\n" + attraction.getContent(), attraction.getExtraImages());
        }
    }

    private void creteExtraImagesHolder(String text, List<String> extraImages) {

        text = text.replace("{", "").replace("}", "");
        String[] sections = text.split("image");

        for (int i = 0; i < sections.length; i++) {
            String section = sections[i];
            TextView textView = new TextView(getActivity());
            textView.setBackgroundColor(getResources().getColor(R.color.white));
            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
            textView.setLayoutParams(params);
            textView.setTypeface(Arial.getInstance(getActivity()).getTypeFace());
            textView.setText(Html.fromHtml(section));
            textView.setMovementMethod(LinkMovementMethod.getInstance());
            extraImagesHolder.addView(textView);

            if (i < extraImages.size()) {
                String image = extraImages.get(i);
                if (!image.contains("www.city-go.eu")) {
                    image = "http://www.city-go.eu" + image;
                    Log.i("contains", "true");
                }

                final ImageView extraImageView = new ImageView(getActivity());
                int height = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 200, getResources().getDisplayMetrics());
                params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
                extraImageView.setLayoutParams(params);
                extraImageView.setBackgroundColor(getResources().getColor(R.color.white));
                extraImageView.setScaleType(ImageView.ScaleType.CENTER_CROP);
                extraImagesHolder.addView(extraImageView);

                // TODO

                try {
                    String url = URLDecoder.decode(image, "UTF-8");
                    Log.i("decode", url);
                    if (URLUtil.isValidUrl(url))
                        ImageLoader.getInstance().displayImage(url, extraImageView, new ImageLoadingListener() {

                            @Override
                            public void onLoadingStarted(String arg0, View arg1) {
                                // TODO Auto-generated method stub

                            }

                            @Override
                            public void onLoadingFailed(String arg0, View arg1, FailReason arg2) {
                                extraImageView.setVisibility(View.GONE);

                            }

                            @Override
                            public void onLoadingComplete(String arg0, View arg1, Bitmap arg2) {
                                // TODO Auto-generated method stub

                            }

                            @Override
                            public void onLoadingCancelled(String arg0, View arg1) {
                                extraImageView.setVisibility(View.GONE);

                            }
                        });
                    else
                        extraImageView.setVisibility(View.GONE);
                } catch (Exception e) {
                    extraImageView.setVisibility(View.GONE);
                }
            }
        }
    }


    @Override
    protected void initGraphicComponents() {
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.start_navigation:
                openNavigation();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void openNavigation() {
        if (attraction.getLat() != 0.0 && attraction.getLon() != 0.0) {
            String navPath = "http://maps.google.com/maps?";
            StringBuilder sb = new StringBuilder();
            sb.append(navPath);
            sb.append("saddr=");
            sb.append(from[0] + "," + from[1]);
            sb.append("&daddr=");
            sb.append(attraction.getLat());
            sb.append(",");
            sb.append(attraction.getLon());
            Intent navigation = new Intent(Intent.ACTION_VIEW, Uri.parse(sb.toString()));
            startActivity(navigation);
        } else {
            Toast.makeText(getActivity().getApplicationContext(), getResources().getString(R.string.map_no_location_for_attraction), Toast.LENGTH_LONG).show();
        }

    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.cupon_details_menu, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

}
