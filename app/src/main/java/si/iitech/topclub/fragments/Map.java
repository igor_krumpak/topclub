package si.iitech.topclub.fragments;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONObject;


import si.iitech.library.rest.RestCall;
import si.iitech.library.rest.RestEnum;
import si.iitech.library.rest.RestResponce;
import si.iitech.topclub.R;
import si.iitech.topclub.common.RestPath;
import si.iitech.topclub.common.Values;
import si.iitech.topclub.object.Attraction;
import si.iitech.topclub.object.Cupon;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Color;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.LoaderManager.LoaderCallbacks;
import android.support.v4.content.Loader;
import android.util.Log;
import android.view.*;
import android.widget.TextView;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.GoogleMap.InfoWindowAdapter;
import com.google.android.gms.maps.GoogleMap.OnInfoWindowClickListener;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.PolylineOptions;

public class Map extends SupportMapFragment implements LoaderCallbacks<RestResponce>, OnInfoWindowClickListener {

    private ChangeFragment notifier = null;
    private GoogleMap map;
    private double[] from = null;
    private double[] to = null;
    private HashMap<String, Cupon> cuponsOnMap = null;
    private HashMap<String, Attraction> attractionsOnMap = null;
    private LatLng fromCoordinates = null;
    private LatLng toCoordinates = null;
    private static String TAG = "Map";

    private boolean loadCupons = false;
    private boolean loadAttractions = false;

    // private ProgressDialog progressDialog = null;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        return super.onCreateView(inflater, container, savedInstanceState);
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            notifier = (ChangeFragment) activity;
        } catch (ClassCastException e) {
        }
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        setHasOptionsMenu(true);

        to = (double[]) getArguments().getSerializable(Values.LOCATION_TO);
        from = (double[]) getArguments().getSerializable(Values.LOCATION_FROM);

        map = getMap();
        map.setInfoWindowAdapter(new InfoWindowAdapter() {
            @Override
            public View getInfoWindow(Marker marker) {
                return null;
            }

            @Override
            public View getInfoContents(Marker marker) {
                LayoutInflater inflater = (LayoutInflater) getActivity().getApplicationContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                View v = inflater.inflate(R.layout.custom_info_window, null);
                TextView data = (TextView) v.findViewById(R.id.textView_data);
                data.setText(marker.getTitle());
                return v;
            }
        });

        fromCoordinates = new LatLng(from[0], from[1]);
        toCoordinates = new LatLng(to[0], to[1]);

        map.setOnInfoWindowClickListener(this);
        cuponsOnMap = new HashMap<String, Cupon>();
        attractionsOnMap = new HashMap<String, Attraction>();


        String type = (String) getArguments().getString(Values.TYPE);

        if(type.equalsIgnoreCase(Values.TYPE_CUPONS)) {
            loadCupons("");
        } else if(type.equalsIgnoreCase(Values.TYPE_ATTRACTIONS)) {
            loadAttractions();
        } else {
            loadCupons("");
        }
    }

    private void loadCupons(String internalCategory) {
        loadAttractions = true;
        loadCupons = false;
        Log.i(TAG, "loadCupons()");
        String category = getArguments().getString(Values.CATEGORY);
        String discount = getArguments().getString(Values.DISCOUNT);
        if(!internalCategory.isEmpty())
            category = internalCategory;
        String rest = RestPath.createCuponPath(category, discount);
        Bundle args = new Bundle();
        args.putParcelable(RestCall.URI_REST, Uri.parse(rest));
        args.putString(RestCall.EXTRA, "Cupons");
        getLoaderManager().restartLoader(RestCall.NALOZI_REST, args, this);

    }

    private void loadAttractions() {
        loadAttractions = true;
        loadCupons = false;
        Log.i(TAG, "loadAttractions()");
        String rest = RestPath.createAttractionPath();
        Bundle args = new Bundle();
        args.putParcelable(RestCall.URI_REST, Uri.parse(rest));
        args.putString(RestCall.EXTRA, "Attractions");
        getLoaderManager().restartLoader(RestCall.NALOZI_REST, args, this);

    }

    @Override
    public Loader<RestResponce> onCreateLoader(int arg0, Bundle arg1) {
        Uri url = arg1.getParcelable(RestCall.URI_REST);

        return RestCall.createRestService(this.getActivity(), RestEnum.GET, url, arg1.getString(RestCall.EXTRA));
    }

    @Override
    public void onLoadFinished(Loader<RestResponce> arg0, RestResponce responce) {
        try {
            if (responce.getStatusCode() == RestCall.httpStatusOK) {
                String data = responce.getData();
                if (!responce.getData().equalsIgnoreCase("null")) {

                    if (responce.getExtra().equalsIgnoreCase("Cupons")) {
                        JSONArray array = new JSONArray(data);
                        List<Cupon> list = Cupon.parseJsonCupons(array);
                        displayCuponsOnMap(list);
                    } else if (responce.getExtra().equalsIgnoreCase("Attractions")) {
                        JSONArray array = new JSONArray(data);
                        List<Attraction> list = Attraction.parseJsonAttractions(array);
                        displayAttractionsOnMap(list);
                    }
                } else {
                }
            } else {
            }
        } catch (Exception e) {
        }

        getActivity().getLoaderManager().destroyLoader(0);
    }

    private void displayAttractionsOnMap(List<Attraction> list) {
        Log.i(TAG, "displayAttractionsOnMap");
        if (to[0] == 0.0 && to[1] == 0.0) {
            map.addMarker(new MarkerOptions().position(fromCoordinates).title(getResources().getString(R.string.map_location_marker))
                    .icon(BitmapDescriptorFactory.fromResource(R.drawable.map_start_position)));
            map.moveCamera(CameraUpdateFactory.newLatLngZoom(fromCoordinates, 12));
        } else {
            map.addMarker(new MarkerOptions().position(fromCoordinates).title(getResources().getString(R.string.map_from_location))
                    .icon(BitmapDescriptorFactory.fromResource(R.drawable.map_start_position)));
            map.addMarker(new MarkerOptions().position(toCoordinates).title(getResources().getString(R.string.map_to_location))
                    .icon(BitmapDescriptorFactory.fromResource(R.drawable.map_end_position)));
            String urlTopass = makeURL(from[0], from[1], to[0], to[1]);
            new connectAsyncTask(urlTopass).execute();
            displayCuponsOnRoute(fromCoordinates, toCoordinates);
        }

        // Display cupons on map
        List<String> userCoordinates = new ArrayList<String>();

        for (int i = 0; i < list.size(); i++) {
            Attraction temp = list.get(i);
            double lat = temp.getLat();
            double lon = temp.getLon();

            Log.i("lat", lat + "");
            Log.i("lon", lon + "");

            while (userCoordinates.contains(String.valueOf(lat) + String.valueOf(lon))) {
                lat = lat + 0.0001;
                lon = lon + 0.0001;
            }
            userCoordinates.add(String.valueOf(lat) + String.valueOf(lon));

            if (lat == 0.0 && lon == 0) {
                continue;
            }
            String name = temp.getTitle();
            LatLng latlon = new LatLng(lat, lon);
            String location = temp.getLocation();
            map.addMarker(new MarkerOptions().position(latlon).title(name).snippet(location)
                    .icon(BitmapDescriptorFactory.fromResource(R.drawable.map_attraction_marker)));
            attractionsOnMap.put(temp.getTitle(), temp);
        }
    }

    private void displayCuponsOnMap(List<Cupon> list) {
        Log.i(TAG, "displayCuponsOnMap");
        if (to[0] == 0.0 && to[1] == 0.0) {
            map.addMarker(new MarkerOptions().position(fromCoordinates).title(getResources().getString(R.string.map_location_marker))
                    .icon(BitmapDescriptorFactory.fromResource(R.drawable.map_start_position)));
            map.moveCamera(CameraUpdateFactory.newLatLngZoom(fromCoordinates, 12));
        } else {
            map.addMarker(new MarkerOptions().position(fromCoordinates).title(getResources().getString(R.string.map_from_location))
                    .icon(BitmapDescriptorFactory.fromResource(R.drawable.map_start_position)));
            map.addMarker(new MarkerOptions().position(toCoordinates).title(getResources().getString(R.string.map_to_location))
                    .icon(BitmapDescriptorFactory.fromResource(R.drawable.map_end_position)));
            String urlTopass = makeURL(from[0], from[1], to[0], to[1]);
            new connectAsyncTask(urlTopass).execute();
            displayCuponsOnRoute(fromCoordinates, toCoordinates);
        }

        // Display cupons on map
        List<String> userCoordinates = new ArrayList<String>();

        for (int i = 0; i < list.size(); i++) {
            Cupon temp = list.get(i);
            double lat = temp.getLat();
            double lon = temp.getLon();

            Log.i("lat", lat + "");
            Log.i("lon", lon + "");

            while (userCoordinates.contains(String.valueOf(lat) + String.valueOf(lon))) {
                lat = lat + 0.0001;
                lon = lon + 0.0001;
            }
            userCoordinates.add(String.valueOf(lat) + String.valueOf(lon));

            if (lat == 0.0 && lon == 0) {
                continue;
            }
            String name = temp.getName();
            LatLng latlon = new LatLng(lat, lon);
            String location = temp.getLocation();
            map.addMarker(new MarkerOptions().position(latlon).title(name).snippet(location)
                    .icon(BitmapDescriptorFactory.fromResource(R.drawable.map_cupon_marker)));
            cuponsOnMap.put(temp.getName(), temp);
        }
    }

    @Override
    public void onLoaderReset(Loader<RestResponce> arg0) {

    }

    public void displayCuponsOnRoute(LatLng from, LatLng to) {
        LatLngBounds.Builder builder = new LatLngBounds.Builder();
        builder.include(from);
        builder.include(to);
        LatLngBounds bounds = builder.build();
        map.moveCamera(CameraUpdateFactory.newLatLngBounds(bounds, 0));
    }

    @Override
    public void onInfoWindowClick(Marker marker) {
        Cupon cupon = cuponsOnMap.get(marker.getTitle());
        if (cupon != null) {
            Bundle bundle = new Bundle();
            bundle.putSerializable(Values.CUPON, cupon);
            bundle.putSerializable(Values.LOCATION_FROM, from);
            notifier.changeFragment(TopClubFragment.CUPON_DETAILS, bundle, false);
            return;
        }

        Attraction attraction = attractionsOnMap.get(marker.getTitle());
        if (attraction != null) {
            Bundle bundle = new Bundle();
            bundle.putSerializable(Values.ATTRACTION, attraction);
            bundle.putSerializable(Values.LOCATION_FROM, from);
            notifier.changeFragment(TopClubFragment.ATTRACTION_DETAILS, bundle, false);
            return;
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    private class connectAsyncTask extends AsyncTask<Void, Void, String> {
        String url;
        ProgressDialog progressDialogRoute;

        connectAsyncTask(String urlPass) {
            url = urlPass;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialogRoute = new ProgressDialog(getActivity());
            progressDialogRoute.setIndeterminate(true);
            progressDialogRoute.setMessage(getString(R.string.map_fetching_route));
            progressDialogRoute.show();
        }

        @Override
        protected String doInBackground(Void... params) {
            JSONParser jParser = new JSONParser();
            String json = jParser.getJSONFromUrl(url);
            return json;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            progressDialogRoute.hide();
            if (result != null) {
                drawPath(result);
            }
        }
    }

    public String makeURL(double sourcelat, double sourcelog, double destlat, double destlog) {
        StringBuilder urlString = new StringBuilder();
        urlString.append("http://maps.googleapis.com/maps/api/directions/json");
        urlString.append("?origin=");// from
        urlString.append(Double.toString(sourcelat));
        urlString.append(",");
        urlString.append(Double.toString(sourcelog));
        urlString.append("&destination=");// to
        urlString.append(Double.toString(destlat));
        urlString.append(",");
        urlString.append(Double.toString(destlog));
        urlString.append("&sensor=false&mode=driving&alternatives=true");
        return urlString.toString();
    }

    public class JSONParser {

        InputStream is = null;
        JSONObject jObj = null;
        String json = "";

        // constructor
        public JSONParser() {
        }

        public String getJSONFromUrl(String url) {

            // Making HTTP request
            try {
                // defaultHttpClient
                DefaultHttpClient httpClient = new DefaultHttpClient();
                HttpPost httpPost = new HttpPost(url);

                HttpResponse httpResponse = httpClient.execute(httpPost);
                HttpEntity httpEntity = httpResponse.getEntity();
                is = httpEntity.getContent();

            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            } catch (ClientProtocolException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
            try {
                BufferedReader reader = new BufferedReader(new InputStreamReader(is, "iso-8859-1"), 8);
                StringBuilder sb = new StringBuilder();
                String line = null;
                while ((line = reader.readLine()) != null) {
                    sb.append(line + "\n");
                }

                json = sb.toString();
                is.close();
            } catch (Exception e) {
            }
            return json;

        }
    }

    public void drawPath(String result) {
        // if (line != null) {
        // map.clear();
        // }
        // map.addMarker(new MarkerOptions().position(endLatLng).icon(
        // BitmapDescriptorFactory.fromResource(R.drawable.redpin_marker)));
        // map.addMarker(new MarkerOptions().position(startLatLng).icon(
        // BitmapDescriptorFactory.fromResource(R.drawable.redpin_marker)));
        try {
            // Tranform the string into a json object
            final JSONObject json = new JSONObject(result);
            JSONArray routeArray = json.getJSONArray("routes");
            JSONObject routes = routeArray.getJSONObject(0);
            JSONObject overviewPolylines = routes.getJSONObject("overview_polyline");
            String encodedString = overviewPolylines.getString("points");
            List<LatLng> list = decodePoly(encodedString);

            PolylineOptions options = new PolylineOptions().width(5).color(Color.BLUE).geodesic(true);
            for (int z = 0; z < list.size() - 1; z++) {
                LatLng point = list.get(z);
                options.add(point);
                // LatLng src = list.get(z);
                // LatLng dest = list.get(z + 1);
                // line = map.addPolyline(new PolylineOptions().add(new
                // LatLng(src.latitude, src.longitude), new
                // LatLng(dest.latitude, dest.longitude)).width(5)
                // .color(Color.BLUE).geodesic(true));
            }

            map.addPolyline(options);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private List<LatLng> decodePoly(String encoded) {

        List<LatLng> poly = new ArrayList<LatLng>();
        int index = 0, len = encoded.length();
        int lat = 0, lng = 0;

        while (index < len) {
            int b, shift = 0, result = 0;
            do {
                b = encoded.charAt(index++) - 63;
                result |= (b & 0x1f) << shift;
                shift += 5;
            } while (b >= 0x20);
            int dlat = ((result & 1) != 0 ? ~(result >> 1) : (result >> 1));
            lat += dlat;

            shift = 0;
            result = 0;
            do {
                b = encoded.charAt(index++) - 63;
                result |= (b & 0x1f) << shift;
                shift += 5;
            } while (b >= 0x20);
            int dlng = ((result & 1) != 0 ? ~(result >> 1) : (result >> 1));
            lng += dlng;

            LatLng p = new LatLng((((double) lat / 1E5)), (((double) lng / 1E5)));
            poly.add(p);
        }

        return poly;
    }

    @Override
    public void onCreateOptionsMenu(android.view.Menu menu, MenuInflater inflater) {
        menu.clear();
        inflater.inflate(R.menu.map_menu, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.show_discounts_and_atractions:
                map.clear();
                AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                builder.setItems(R.array.categories_array, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        loadCupons(getResources().getStringArray(R.array.categories_array_values)[which]);
                    }
                });
                builder.show();
                loadAttractions();
                return true;
            default:
                return super.onOptionsItemSelected(item);

        }
    }

}
