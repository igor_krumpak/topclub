package si.iitech.topclub.fragments;


import si.iitech.library.fragment.IITechFragment;
import si.iitech.library.rest.RestResponce;
import si.iitech.library.util.IITechUtil;
import si.iitech.topclub.R;
import si.iitech.topclub.common.Arial;
import si.iitech.topclub.common.Values;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.v4.app.LoaderManager.LoaderCallbacks;
import android.support.v4.content.Loader;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RadioGroup.OnCheckedChangeListener;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

public class CuponFilterOnTheWay extends IITechFragment implements LoaderCallbacks<RestResponce>, OnClickListener, OnCheckedChangeListener, LocationListener {

	protected Spinner			spinnerCategories	= null;
	protected Spinner			spinnerDiscounts	= null;
	protected Spinner			spinnerShowAs		= null;
	protected ChangeFragment	notifier			= null;
	protected TextView			textViewFrom		= null;
	protected TextView			textViewTo			= null;
	protected EditText			editTextAddressFrom	= null;
	protected EditText			editTextAddressTo	= null;
	protected Button			buttonSearch		= null;
	protected Button			buttonReset			= null;
	protected RadioGroup		radioGroupAddress	= null;
	protected RadioButton		radioButtonPosition	= null;
	protected RadioButton		radioButtonAddress	= null;
	private ProgressDialog		progressDialog		= null;

	private final int			showAsList			= 0;

	private LocationManager		locationManager		= null;

	@Override
	public void onAttach(Activity activity) {
		super.onAttach(activity);
		try {
			notifier = (ChangeFragment) activity;
		} catch (ClassCastException e) {
		}
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
		setHasOptionsMenu(true);
	}

	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.cupon_filter, container, false);

		spinnerCategories = (Spinner) view.findViewById(R.id.cupon_filter_categories);
		ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(getActivity(), R.array.categories_array, android.R.layout.simple_spinner_item);
		adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		spinnerCategories.setAdapter(adapter);

		spinnerDiscounts = (Spinner) view.findViewById(R.id.cupon_filter_discount);
		ArrayAdapter<CharSequence> adapter1 = ArrayAdapter.createFromResource(getActivity(), R.array.discount_array, android.R.layout.simple_spinner_item);
		adapter1.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		spinnerDiscounts.setAdapter(adapter1);

		spinnerShowAs = (Spinner) view.findViewById(R.id.cupon_filter_show_as);
		ArrayAdapter<CharSequence> adapter2 = ArrayAdapter.createFromResource(getActivity(), R.array.show_as_array, android.R.layout.simple_spinner_item);
		adapter2.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		spinnerShowAs.setAdapter(adapter2);
		spinnerShowAs.setSelection(1);

		textViewFrom = (TextView) view.findViewById(R.id.cupon_filter_text_view_from);
		textViewFrom.setTypeface(Arial.getInstance(getActivity()).getTypeFace());
		textViewTo = (TextView) view.findViewById(R.id.cupon_filter_text_view_to);
		textViewTo.setTypeface(Arial.getInstance(getActivity()).getTypeFace());
		editTextAddressFrom = (EditText) view.findViewById(R.id.cupon_filter_address_from);
		editTextAddressFrom.setTypeface(Arial.getInstance(getActivity()).getTypeFace());
		editTextAddressTo = (EditText) view.findViewById(R.id.cupon_filter_address_to);
		editTextAddressTo.setTypeface(Arial.getInstance(getActivity()).getTypeFace());

		buttonReset = (Button) view.findViewById(R.id.cupon_filter_button_reset);
		buttonReset.setOnClickListener(this);
		buttonReset.setTypeface(Arial.getInstance(getActivity()).getTypeFace());
		buttonSearch = (Button) view.findViewById(R.id.cupon_filter_button_submit);
		buttonSearch.setOnClickListener(this);
		buttonSearch.setTypeface(Arial.getInstance(getActivity()).getTypeFace());

		radioGroupAddress = (RadioGroup) view.findViewById(R.id.cupon_filter_radio_group_navigate_to);
		radioGroupAddress.setOnCheckedChangeListener(this);

		radioButtonPosition = (RadioButton) view.findViewById(R.id.cupon_filter_radio_button_position);
		radioButtonPosition.setButtonDrawable(R.drawable.color_radiobutton);

		radioButtonAddress = (RadioButton) view.findViewById(R.id.cupon_filter_radio_button_address);
		radioButtonAddress.setButtonDrawable(R.drawable.color_radiobutton);

		locationManager = (LocationManager) getActivity().getSystemService(Context.LOCATION_SERVICE);

		progressDialog = new ProgressDialog(getActivity());
		progressDialog.setMessage(getString(R.string.location_fetching));
		progressDialog.setIndeterminate(true);

		return view;
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.cupon_filter_button_reset:
			editTextAddressFrom.setText("");
			editTextAddressTo.setText("");
			spinnerCategories.setSelection(0);
			spinnerDiscounts.setSelection(0);
			spinnerShowAs.setSelection(1);
			break;

		case R.id.cupon_filter_button_submit:

			int id = radioGroupAddress.getCheckedRadioButtonId();
			View radioButton = radioGroupAddress.findViewById(id);
			if (radioButton.getId() == R.id.cupon_filter_radio_button_position) {
				requestLocationUpdates();
			} else {
				double[] from = checkLocation(editTextAddressFrom);
				double[] to = checkLocation(editTextAddressTo);
				if (from != null && to != null) {
					displayResults(from, to);
					break;
				}
			}
		}
	}

	protected void requestLocationUpdates() {
		locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 0, 0, this);
		locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 0, this);
		progressDialog.show();
	}

	protected double[] checkLocation(EditText editText) {
		String location = editText.getText().toString();
		if (location.length() == 0) {
			editText.setError(getResources().getString(R.string.validation_address));
			editText.requestFocus();
			return null;
		} else {
			double[] point = IITechUtil.locationToLonAndLat(getActivity(), location);
			if (point[0] != 0.0 && point[1] != 0.0) {
				return point;
			} else {
				editText.setError(getResources().getString(R.string.validation_valid_address));
				editText.requestFocus();
				return null;
			}
		}
	}

	@Override
	public Loader<RestResponce> onCreateLoader(int arg0, Bundle arg1) {
		return null;
	}

	@Override
	public void onLoadFinished(Loader<RestResponce> arg0, RestResponce arg1) {

	}

	@Override
	public void onLoaderReset(Loader<RestResponce> arg0) {

	}

	@Override
	protected void initGraphicComponents() {
	}

	@Override
	public void onCheckedChanged(RadioGroup group, int checkedId) {
		int id = radioGroupAddress.getCheckedRadioButtonId();
		View radioButton = radioGroupAddress.findViewById(id);
		if (radioButton.getId() == R.id.cupon_filter_radio_button_position) {
			editTextAddressFrom.setEnabled(false);
			editTextAddressFrom.setText("");
		} else {
			editTextAddressFrom.setEnabled(true);
		}
	}

	@Override
	public void onLocationChanged(Location location) {
		locationManager.removeUpdates(this);
		progressDialog.hide();
		displayResults(location);
	}

	@Override
	public void onProviderDisabled(String provider) {
		try {
			Location location = locationManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
			if (location != null) {
				locationManager.removeUpdates(this);
				displayResults(location);
				progressDialog.hide();
			} else {
				Toast.makeText(getActivity(), getResources().getString(R.string.error_no_location), Toast.LENGTH_LONG).show();
				locationManager.removeUpdates(this);
				progressDialog.hide();
			}
		} catch (Exception e) {
			Toast.makeText(getActivity(), getResources().getString(R.string.error_no_location) + " 1", Toast.LENGTH_LONG).show();
		}

	}

	@Override
	public void onProviderEnabled(String arg0) {

	}

	@Override
	public void onStatusChanged(String arg0, int arg1, Bundle arg2) {

	}

	protected void displayResults(Location location) {
		double from[] = new double[] { location.getLatitude(), location.getLongitude() };
		double to[] = checkLocation(editTextAddressTo);
		if (to != null) {
			displayResults(from, to);
		}
	}

	protected void displayResults(double[] from, double[] to) {
		Bundle bundle = new Bundle();
		bundle.putSerializable(Values.LOCATION_FROM, from);
		bundle.putSerializable(Values.LOCATION_TO, to);
		bundle.putString(Values.CATEGORY, getResources().getStringArray(R.array.categories_array_values)[spinnerCategories.getSelectedItemPosition()]);
		bundle.putString(Values.DISCOUNT, getResources().getStringArray(R.array.discount_array_values)[spinnerDiscounts.getSelectedItemPosition()]);
		if (spinnerShowAs.getSelectedItemPosition() == showAsList) {
			notifier.changeFragment(TopClubFragment.CUPON_LIST, bundle, false);
		} else {
			bundle.putString(Values.TYPE, Values.TYPE_CUPONS);
			notifier.changeFragment(TopClubFragment.MAP, bundle, false);
		}
	}

}
