package si.iitech.topclub.fragments;

import android.app.Activity;
import android.app.AlertDialog;
import android.location.Location;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.Loader;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ListView;

import org.json.JSONArray;

import java.util.Collections;
import java.util.List;

import si.iitech.library.fragment.IITechListFragment;
import si.iitech.library.rest.RestCall;
import si.iitech.library.rest.RestEnum;
import si.iitech.library.rest.RestResponce;
import si.iitech.topclub.R;
import si.iitech.topclub.adapter.AttractionAdapter;
import si.iitech.topclub.common.RestPath;
import si.iitech.topclub.common.Values;
import si.iitech.topclub.object.Attraction;

public class AttractionList extends IITechListFragment implements LoaderManager.LoaderCallbacks<RestResponce> {

    private ChangeFragment notifier = null;
    private List<Attraction> list = null;
    private double[] from = null;
    private String[] realValues = null;
    private String[] displayValues = null;

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            notifier = (ChangeFragment) activity;
        } catch (ClassCastException e) {
        }
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        getListView().setDivider(null);
        getListView().setDividerHeight(0);
        getListView().setBackgroundColor(getResources().getColor(R.color.gray));
        setHasOptionsMenu(true);
        from = (double[]) getArguments().getSerializable(Values.LOCATION_FROM);


        String rest = RestPath.createAttractionPath();
        Bundle args = new Bundle();
        Log.i("GET CUPONS", rest);
        args.putParcelable(RestCall.URI_REST, Uri.parse(rest));
        getLoaderManager().initLoader(RestCall.NALOZI_REST, args, this);
    }

    @Override
    public void onListItemClick(ListView l, View v, int position, long id) {
        Attraction attraction = list.get(position);
        Bundle bundle = new Bundle();
        bundle.putSerializable(Values.ATTRACTION, attraction);
        bundle.putSerializable(Values.LOCATION_FROM, from);
        notifier.changeFragment(TopClubFragment.ATTRACTION_DETAILS, bundle, false);
    }

    @Override
    public Loader<RestResponce> onCreateLoader(int arg0, Bundle arg1) {
        Uri url = arg1.getParcelable(RestCall.URI_REST);
        return new RestCall(this.getActivity(), RestEnum.GET, url, "", "");
    }

    @Override
    public void onLoadFinished(Loader<RestResponce> arg0, RestResponce responce) {
        try {
            if (responce.getStatusCode() == RestCall.httpStatusOK) {
                String data = responce.getData();
                if (!responce.getData().equalsIgnoreCase("null")) {
                    JSONArray array = new JSONArray(data);
                    list = Attraction.parseJsonAttractions(array);
                    if (from[0] != 0.0 && from[1] != 0.0) {
                        sortByDistance(list);
                    }
                    AttractionAdapter adapter = new AttractionAdapter(getActivity().getApplicationContext(), list);
                    updateListAdapter(adapter);

                } else {
                    this.setListAdapter(null);
                    setEmptyText(getResources().getString(R.string.error_no_data));
                }
            } else {
                this.setListAdapter(null);
                setEmptyText(getResources().getString(R.string.error_no_connection));
            }
        } catch (Exception e) {
            this.setListAdapter(null);
            setEmptyText(getResources().getString(R.string.error));
        }
    }

    protected void updateListAdapter(AttractionAdapter adapter) {
        this.setListAdapter(adapter);
    }

    @Override
    public void onLoaderReset(Loader<RestResponce> arg0) {

    }

    private void sortByDistance(List<Attraction> list) {
        Location myLocation = new Location("");
        myLocation.setLatitude(from[0]);
        myLocation.setLongitude(from[1]);
        Location toLocation;
        for (Attraction cupon : list) {
            toLocation = new Location("");
            toLocation.setLatitude(cupon.getLat());
            toLocation.setLongitude(cupon.getLon());
            double distance = myLocation.distanceTo(toLocation);
            cupon.setDistance(distance);
        }
        Collections.sort(list);
        // for (Cupon cupon : list) {
        // Log.d("DISTANCE SORTED", cupon.getDistance() + " ");
        // }

    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.cupon_list_map_only_menu, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.show_map:
                showMap();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }


    private void showMap() {
        Bundle bundle = new Bundle();
        double to[] = new double[2];
        bundle.putSerializable(Values.LOCATION_TO, to);
        bundle.putSerializable(Values.LOCATION_FROM, from);
        bundle.putString(Values.TYPE, Values.TYPE_ATTRACTIONS);
        bundle.putString(Values.CATEGORY, "0");
        bundle.putString(Values.DISCOUNT, "0");
        notifier.changeFragment(TopClubFragment.MAP, bundle, false);
    }


}
