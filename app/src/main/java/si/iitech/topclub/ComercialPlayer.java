package si.iitech.topclub;

import java.util.Random;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;


import si.iitech.library.activity.IITechActivity;
import si.iitech.library.rest.RestCall;
import si.iitech.library.rest.RestEnum;
import si.iitech.library.rest.RestResponce;
import si.iitech.topclub.common.RestPath;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.Loader;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.VideoView;

public class ComercialPlayer extends IITechActivity implements LoaderManager.LoaderCallbacks<RestResponce>, Runnable {

	private VideoView	myVideoView			= null;
	private Button		close				= null;
	private boolean		videoEnded			= false;
	private TextView	loadingText			= null;
	private final int	timeToComercialEnd	= 10000;
	private Handler		handler				= null;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.comercial_player);

		myVideoView = (VideoView) findViewById(R.id.videoView_player);
		close = (Button) findViewById(R.id.button_close_comercial);
		close.setVisibility(View.INVISIBLE);
		loadingText = (TextView) findViewById(R.id.textView_comercial_info);

		startRest();

	}

	private void startRest() {
		Bundle args = new Bundle();
		args.putParcelable(RestCall.URI_REST, Uri.parse(RestPath.comercialPath()));
		getSupportLoaderManager().initLoader(RestCall.NALOZI_REST, args, this);
	}

	@Override
	protected void initGraphicComponents() {
		// TODO Auto-generated method stub
	}

	@Override
	public void onBackPressed() {
		if (videoEnded) {
			super.onBackPressed();
		}
	}

	public void endComercial(View v) {
		this.finish();
	}

	@Override
	public Loader<RestResponce> onCreateLoader(int arg0, Bundle arg1) {
		Uri url = arg1.getParcelable(RestCall.URI_REST);
		return new RestCall(this, RestEnum.GET, url, "", "");
	}

	@Override
	public void onLoadFinished(Loader<RestResponce> arg0, RestResponce responce) {
		if (responce.getStatusCode() == RestCall.httpStatusOK) {
			String data = responce.getData();
			if (!responce.getData().equalsIgnoreCase("null")) {
				try {
					JSONArray array = new JSONArray(data);
					Random random = new Random();
					if(array.length() > 0) {
						int i = random.nextInt(array.length());
						JSONObject json = array.getJSONObject(i);
						String video = json.getString("video");
						playVideo(video);
					} else {
						this.finish();
					}

				} catch (JSONException e) {
					//Toast.makeText(this, getResources().getString(R.string.error_comercial), Toast.LENGTH_LONG).show();
					this.finish();
				}
			} else {
				//Toast.makeText(this, getResources().getString(R.string.error_comercial), Toast.LENGTH_LONG).show();
				this.finish();
			}
		} else {
			//Toast.makeText(this, getResources().getString(R.string.error_comercial), Toast.LENGTH_LONG).show();
			this.finish();
		}

	}

	private void playVideo(String video) {
		if (video.length() > 0) {
			myVideoView.setVideoURI(Uri.parse(video));
			myVideoView.start();
			handler = new Handler();
			handler.postDelayed(this, timeToComercialEnd);
		} else {
			Toast.makeText(this, getResources().getString(R.string.error_comercial), Toast.LENGTH_LONG).show();
			this.finish();
		}
	}

	@Override
	public void onLoaderReset(Loader<RestResponce> arg0) {
		// TODO Auto-generated method stub
	}

	@Override
	protected void onStop() {
		super.onStop();
		if(handler != null) {
			handler.removeCallbacks(this);
		}
		this.finish();
	}

	@Override
	public void run() {
		close.setVisibility(View.VISIBLE);
		loadingText.setVisibility(View.INVISIBLE);
		videoEnded = true;
		
	}
}
