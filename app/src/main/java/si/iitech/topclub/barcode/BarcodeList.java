package si.iitech.topclub.barcode;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;

import si.iitech.library.fragment.IITechFragment;
import si.iitech.library.fragment.IITechListFragment;
import si.iitech.topclub.R;
import si.iitech.topclub.adapter.BarcodesAdapter;
import si.iitech.topclub.fragments.ChangeFragment;
import si.iitech.topclub.fragments.TopClubFragment;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.AdapterView.OnItemLongClickListener;
import android.widget.ListView;

public class BarcodeList extends IITechFragment implements OnItemLongClickListener, AdapterView.OnItemClickListener{

	protected ChangeFragment	notifier	= null;
	private List<Barcode>		list;
	private boolean				deleteMode	= false;
	private ListView            listView;

	@Override
	public void onAttach(Activity activity) {
		super.onAttach(activity);
		try {
			notifier = (ChangeFragment) activity;
		} catch (ClassCastException e) {
		}
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.list_view, container, false);
		listView = (ListView)  view.findViewById(R.id.my_list_view);
		listView.setDivider(null);
		listView.setDividerHeight(0);
		listView.setBackgroundColor(getResources().getColor(R.color.gray));
		listView.setOnItemLongClickListener(this);
		listView.setOnItemClickListener(this);
		return view;
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
		setHasOptionsMenu(true);
		refresh();
	}



	@Override
	public void onViewStateRestored(Bundle savedInstanceState) {
		refresh();
		super.onViewStateRestored(savedInstanceState);
	}

	public void refresh() {
		list = Barcode.getScans(getActivity());
		if (list == null || list.size() == 0) {
			listView.setAdapter(null);
		} else {
			BarcodesAdapter adapter = new BarcodesAdapter(getActivity(), list);
			listView.setAdapter(adapter);
		}
	}

	@Override
	public boolean onItemLongClick(AdapterView<?> arg0, View arg1, int position, long arg3) {
		if (deleteMode) {
			Barcode.deleteScan(getActivity(), position);
			refresh();
			deleteMode = false;
		}
		return false;
	}

	@Override
	public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
		inflater.inflate(R.menu.barcodes_menu, menu);
		super.onCreateOptionsMenu(menu, inflater);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case R.id.new_barcode:
			notifier.changeFragment(TopClubFragment.NEW_BAR_CODE_STEP_ONE, null, false);
			return true;
		case R.id.delete_barcode:
			Toast.makeText(getActivity(), getResources().getString(R.string.alert_delete_barcode), Toast.LENGTH_LONG).show();
			deleteMode = true;
			return true;
		default:
			return super.onOptionsItemSelected(item);
		}
	}
	
	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		// TODO Auto-generated method stub
		super.onActivityResult(requestCode, resultCode, data);
	}

	@Override
	protected void initGraphicComponents() {

	}

	@Override
	public void onItemClick(AdapterView<?> adapterView, View view, int position, long l) {
		Barcode barcode = list.get(position);
		try {
			Intent intent = new Intent("com.google.zxing.client.android.ENCODE");
			intent.putExtra("ENCODE_TYPE", "sdfs");
			intent.putExtra("ENCODE_DATA", barcode.getContent());
			intent.putExtra("ENCODE_FORMAT", barcode.getFormat());
			getActivity().startActivity(intent);
		} catch(Exception e) {
			IntentIntegrator.initiateScan(getActivity());
		}
	}
}
