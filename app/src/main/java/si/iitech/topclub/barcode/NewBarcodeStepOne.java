package si.iitech.topclub.barcode;


import si.iitech.library.fragment.IITechFragment;
import si.iitech.library.util.IITechUtil;
import si.iitech.topclub.R;
import si.iitech.topclub.common.Values;
import si.iitech.topclub.fragments.ChangeFragment;
import si.iitech.topclub.fragments.TopClubFragment;
import android.app.Activity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

public class NewBarcodeStepOne extends IITechFragment implements OnClickListener, OnItemSelectedListener {

	protected ChangeFragment	notifier				= null;
	private Spinner				barcodeStores			= null;
	private Barcode				barcode					= null;
	private Button				buttonDeleteBarcode		= null;
	private Button				saveBarcode				= null;
	private EditText			editTextBarcodeTitle	= null;
	private EditText			editTextShopTitle		= null;
	private TextView			showBarcode				= null;

	@Override
	public void onAttach(Activity activity) {
		super.onAttach(activity);
		try {
			notifier = (ChangeFragment) activity;
		} catch (ClassCastException e) {
		}
	}

	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.new_barcode_step_one, container, false);
		barcodeStores = (Spinner) view.findViewById(R.id.barcode_stores);
		ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(getActivity(), R.array.barcode_stores_array, android.R.layout.simple_spinner_item);
		adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		barcodeStores.setAdapter(adapter);
		barcodeStores.setOnItemSelectedListener(this);
		buttonDeleteBarcode = (Button) view.findViewById(R.id.button_delete_barcode);
		buttonDeleteBarcode.setOnClickListener(this);
		saveBarcode = (Button) view.findViewById(R.id.button_save_barcode);
		saveBarcode.setOnClickListener(this);
		editTextBarcodeTitle = (EditText) view.findViewById(R.id.barcode_title);
		editTextShopTitle = (EditText) view.findViewById(R.id.editText_shop_title);
		editTextShopTitle.setVisibility(View.VISIBLE);
		return view;
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		setHasOptionsMenu(true);
		super.onActivityCreated(savedInstanceState);
	}

	public void onClick(View v) {

		switch (v.getId()) {

		case R.id.button_save_barcode:
			if (IITechUtil.checkValues(editTextBarcodeTitle, "", getResources().getString(R.string.validation_barcode_title), null)) {
				Bundle bundle = new Bundle();
				bundle.putSerializable(Values.SCAN_NAME, editTextBarcodeTitle.getText().toString());

				if (editTextShopTitle.getVisibility() == View.VISIBLE) {
					if (IITechUtil.checkValues(editTextShopTitle, "", getResources().getString(R.string.validation_barcode_shop), null)) {
						bundle.putSerializable(Values.SCAN_VALUE, editTextShopTitle.getText().toString());
					} else {
						return;
					}
				} else {
					bundle.putSerializable(Values.SCAN_VALUE, barcodeStores.getSelectedItem().toString());
				}
				notifier.changeFragment(TopClubFragment.NEW_BAR_CODE_STEP_TWO, bundle, false);
			}
			break;
		case R.id.button_delete_barcode:
			notifier.popFragment();
			break;
		}
	}

	@Override
	protected void initGraphicComponents() {
	}


	@Override
	public void onItemSelected(AdapterView<?> arg0, View arg1, int item, long arg3) {
		if (item == 0) {
			editTextShopTitle.setVisibility(View.VISIBLE);
		} else {
			editTextShopTitle.setVisibility(View.INVISIBLE);
		}
	}

	@Override
	public void onNothingSelected(AdapterView<?> arg0) {
		// TODO Auto-generated method stub
	}

	@Override
	public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
		menu.removeItem(R.id.delete_barcode);
		menu.removeItem(R.id.new_barcode);
		super.onCreateOptionsMenu(menu, inflater);
	}



}
