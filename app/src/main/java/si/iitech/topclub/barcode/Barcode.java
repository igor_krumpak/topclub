package si.iitech.topclub.barcode;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import si.iitech.library.util.Serialize;
import si.iitech.topclub.R;
import android.app.Activity;
import android.content.Context;
import android.graphics.drawable.Drawable;

@SuppressWarnings("unchecked")
public class Barcode implements Serializable {

	private static final long	serialVersionUID	= -3012245057007200262L;

	private static final String	file				= "nepovem";
	private static final String	prefix				= ".haha";

	private String				content;
	private String				format;
	private String				shop;
	private String				title;

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public String getFormat() {
		return format;
	}

	public void setFormat(String format) {
		this.format = format;
	}

	public String getShop() {
		return shop;
	}

	public void setShop(String shop) {
		this.shop = shop;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getTitle() {
		return title;
	}

	public static void newScan(Activity activity, Barcode barcode) {
		List<Barcode> list = Serialize.deserialize(activity, List.class, file, prefix);
		if (list == null)
			list = new ArrayList<Barcode>();
		list.add(barcode);
		Serialize.serialize(activity, list, file, prefix);
	}

	public static List<Barcode> getScans(Activity activity) {
		List<Barcode> list = Serialize.deserialize(activity, List.class, file, prefix);
		return list;
	}

	public static void deleteScan(Activity activity, int position) {
		List<Barcode> list = Serialize.deserialize(activity, List.class, file, prefix);
		list.remove(position);
		Serialize.serialize(activity, list, file, prefix);
	}

	public Drawable getImage(Context context) {
		if (shop.equalsIgnoreCase("Jager Trgovine")) {
			return context.getResources().getDrawable(R.drawable.new_jager);
		} else if (shop.equalsIgnoreCase("Mercur")) {
			return context.getResources().getDrawable(R.drawable.new_merkur);
		} else if (shop.equalsIgnoreCase("Spar")) {
			return context.getResources().getDrawable(R.drawable.new_spar);
		} else if (shop.equalsIgnoreCase("Hervis")) {
			return context.getResources().getDrawable(R.drawable.new_hervis);
		} else if (shop.equalsIgnoreCase("TUS")) {
			return context.getResources().getDrawable(R.drawable.new_tus);
		} else if (shop.equalsIgnoreCase("DM")) {
			return context.getResources().getDrawable(R.drawable.new_dm);
		} else if (shop.equalsIgnoreCase("Metro")) {
			return context.getResources().getDrawable(R.drawable.new_metro);
		} else if (shop.equalsIgnoreCase("Leclerk")) {
			return context.getResources().getDrawable(R.drawable.new_eleclerk);
		} else if (shop.equalsIgnoreCase("Spar")) {
			return context.getResources().getDrawable(R.drawable.new_spar);
		} else if (shop.equalsIgnoreCase("Tukano")) {
			return context.getResources().getDrawable(R.drawable.new_tukano);
		} else if (shop.equalsIgnoreCase("Pikapolonica")) {
			return context.getResources().getDrawable(R.drawable.new_pikapolonica);
		} else if (shop.equalsIgnoreCase("Petrol")) {
			return context.getResources().getDrawable(R.drawable.new_petrol);
		} else {
			return context.getResources().getDrawable(R.drawable.new_ostalo);
		}
	}

}
