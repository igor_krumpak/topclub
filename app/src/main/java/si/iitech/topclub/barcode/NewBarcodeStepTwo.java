package si.iitech.topclub.barcode;


import si.iitech.library.fragment.IITechFragment;
import si.iitech.library.util.IITechUtil;
import si.iitech.topclub.R;
import si.iitech.topclub.common.Values;
import si.iitech.topclub.fragments.ChangeFragment;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class NewBarcodeStepTwo extends IITechFragment implements OnClickListener {

	protected ChangeFragment	notifier			= null;
	private Barcode				barcode				= null;
	private Button				startBarcodeScan	= null;
	private Button				saveBarcode			= null;
	private EditText			editTextScanValue	= null;
	private String				name				= null;
	private String				shop				= null;
	private String				scanContent			= null;
	private String				scanFormat			= "CODE_128";

	@Override
	public void onAttach(Activity activity) {
		super.onAttach(activity);
		try {
			notifier = (ChangeFragment) activity;
		} catch (ClassCastException e) {
		}
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
		name = getArguments().getString(Values.SCAN_NAME);
		shop = getArguments().getString(Values.SCAN_VALUE);
	}

	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

		View view = inflater.inflate(R.layout.new_barcode_step_two, container, false);

		startBarcodeScan = (Button) view.findViewById(R.id.button_start_scan);
		startBarcodeScan.setOnClickListener(this);

		saveBarcode = (Button) view.findViewById(R.id.button_final_save_barcode);
		saveBarcode.setOnClickListener(this);

		editTextScanValue = (EditText) view.findViewById(R.id.editText_scan_value);

		return view;
	}

	public void onClick(View v) {

		switch (v.getId()) {
		case R.id.button_start_scan:
			try {
				IntentIntegrator.initiateScan(getActivity());
			} catch (Exception e) {
			}

			break;

		case R.id.button_final_save_barcode:
			if (IITechUtil.checkValues(editTextScanValue, "", getResources().getString(R.string.validation_barcode_value), null)) {
				barcode = new Barcode();
				barcode.setTitle(name);
				barcode.setShop(shop);
				barcode.setFormat(scanFormat);
				barcode.setContent(editTextScanValue.getText().toString());
				Barcode.newScan(getActivity(), barcode);
				notifier.popFragment();
				notifier.popFragment();
			}
			break;
		}
	}

	@Override
	protected void initGraphicComponents() {
	}

	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent intent) {
		try {
			IntentResult scanningResult = IntentIntegrator.parseActivityResult(requestCode, resultCode, intent);
			if (scanningResult != null) {
				scanContent = scanningResult.getContents();
				scanFormat = scanningResult.getFormatName();
				editTextScanValue.setText(scanContent);
				Log.i("scanFormat", scanFormat);
			} else {
				Toast.makeText(getActivity(), getResources().getString(R.string.scan_error), Toast.LENGTH_LONG).show();
			}
		} catch (Exception e) {
		}
	}
}
