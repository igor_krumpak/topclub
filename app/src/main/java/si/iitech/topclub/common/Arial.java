package si.iitech.topclub.common;

import android.content.Context;
import android.graphics.Typeface;

public class Arial {
	
	private Context context;
	private static Arial instance;
 
	public Arial(Context context) {
		this.context = context;
	}
 
	public static Arial getInstance(Context context) {
		synchronized (Arial.class) {
			if (instance == null)
				instance = new Arial(context);
			return instance;
		}
	}
 
	public Typeface getTypeFace() {
		return Typeface.createFromAsset(context.getResources().getAssets(),
				"arial.ttf");
	}

}
