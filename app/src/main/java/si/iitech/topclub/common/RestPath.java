package si.iitech.topclub.common;


public class RestPath {
	public static String createCuponPath(String category, String discount) {
		String cuponPath = null;
		if ((category.equals("0")) || (discount.equals("0"))) {
			if (category.equals("0")) {
				category = discount;
			}
			String path = "http://www.city-go.eu/restful.php?type=allcoupons&hash=fxq26JjZsPX84h8XHai7YV8YR";
			StringBuilder builder = new StringBuilder();
			builder.append(path);
			builder.append("&category=");
			builder.append(category);
			cuponPath = builder.toString();
		} else if ((category.equals("0")) && (discount.equals("0"))) {
			cuponPath = "http://www.city-go.eu/restful.php?type=allcoupons&hash=fxq26JjZsPX84h8XHai7YV8YR";
		} else {
			String path = "http://www.city-go.eu/restful.php?type=couponstwocat&hash=fxq26JjZsPX84h8XHai7YV8YR";
			StringBuilder builder = new StringBuilder();
			builder.append(path);
			builder.append("&category=");
			builder.append(category);
			builder.append("&min_max=");
			builder.append(discount);
			cuponPath = builder.toString();
		}
		return cuponPath;
	}

    public static String createAttractionPath() {
        return "http://www.city-go.eu/restful.php?type=returnAttractions&hash=fxq26JjZsPX84h8XHai7YV8YR";
    }

	public static String createEkoPath() {
		return "http://www.city-go.eu/restful.php?type=returnKmetije&hash=fxq26JjZsPX84h8XHai7YV8YR";
	}

	public static String createNewsPath(String id) {
		return "http://www.city-go.eu/restful.php?type=returnNews&obcina=" + id + "&hash=fxq26JjZsPX84h8XHai7YV8YR";
	}

	public static String createSpaceInPath(String path) {
		return path.replace(" ", "%20");
	}
	
	public static String makeStringUrlFriendly(String url) {
		url = url.replaceAll("\\%", "%25");
		url = createSpaceInPath(url);
		return url;
	}

	public static String createPicturePath(String picture) {
		return createSpaceInPath("http://www.city-go.eu/image/" + picture);
	}

	public static String comercialPath() {
		return createSpaceInPath("http://www.city-go.eu/restful.php?type=returnADs&hash=fxq26JjZsPX84h8XHai7YV8YR");
	}

	public static String createMyCuponPath(String userId) {
		String path = "http://www.city-go.eu/restful.php?hash=fxq26JjZsPX84h8XHai7YV8YR&type=mojikuponi";
		StringBuilder builder = new StringBuilder();
		builder.append(path);
		builder.append("&id=");
		builder.append(userId);
		return builder.toString();
	}

	public static String createCuponCodePath(int size, int id) {
		String path = "http://www.city-go.eu/script/barcode.php";
		StringBuilder builder = new StringBuilder();
		builder.append(path);
		builder.append("?size=");
		builder.append(size);
		builder.append("&text=");
		builder.append(id);
		return builder.toString();
	}

	public static String createDeleteMyCuponPath(int deleteId) {
		String path = "http://www.city-go.eu/restful.php?type=deletemycoupons&hash=fxq26JjZsPX84h8XHai7YV8YR";
		StringBuilder builder = new StringBuilder();
		builder.append(path);
		builder.append("&id=");
		builder.append(deleteId);
		return builder.toString();
	}

}
