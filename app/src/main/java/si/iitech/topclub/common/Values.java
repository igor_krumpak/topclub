package si.iitech.topclub.common;

public class Values {

	public static final String	KEY						= "fxq26JjZsPX84h8XHai7YV8YR";
	public static final String	APP_NAME				= "TopClub2";
	public static final String	EMAIL_PATTERN			= "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@" + "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";
	public static final String	NUMBERS_PATTERN			= "^[0-9]+$";

	public static final String	EURO_SIGN				= "\u20AC";
	public static final String	PROCENT_SIGN			= "%";
	public static final String	PROCENT_SING_UNICODE	= "\u0025";

	public static final String	SHOW_ATRACTIONS			= "SHOW_ATRACTIONS";
	public static final String	LOCATION_FROM			= "LOCATION_FROM";
	public static final String	LOCATION_TO				= "LOCATION_TO";
	public static final String	CATEGORY				= "CATEGORY";
	public static final String	DISCOUNT				= "DISCOUNT";
	public static final String	CUPON					= "CUPON";
    public static final String	ATTRACTION   			= "ATTRACTION";
	public static final String	EKO   			        = "EKO";
	public static final String	NEWS   			        = "NEWS";

	public static final String	DATE_FORMAT				= "yyyy-mm-dd";
	public static final String	CUPON_ID				= "CUPON_ID";
	public static final String	SCAN_NAME				= "SCAN_NAME";
	public static final String	SCAN_VALUE				= "SCAN_SHOP";


	public static final String TYPE = "type";
	public static final String TYPE_CUPONS = "Cupons";
	public static final String TYPE_ATTRACTIONS = "Attractions";
	public static final java.lang.String REGIJA = "Regija";
}
