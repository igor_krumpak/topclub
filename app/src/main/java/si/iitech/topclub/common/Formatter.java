package si.iitech.topclub.common;

import si.iitech.topclub.R;
import si.iitech.topclub.object.Cupon;
import android.content.Context;
import android.graphics.Paint;
import android.view.View;
import android.widget.TextView;

public class Formatter {

	public static void formatCuponOutput(Context context, TextView oldPrice, TextView specialPrice, Cupon cupon, TextView oldPriceTitle, String procentExtra) {
		if (cupon.getSpecialPrice() == 0.0) {
			specialPrice.setVisibility(View.INVISIBLE);
			oldPrice.setText(procentExtra + cupon.getPrice() + " " + Values.PROCENT_SIGN);
			oldPrice.setPaintFlags(0);
			if(oldPriceTitle != null) {
				oldPriceTitle.setText(context.getResources().getString(R.string.procent_value));
			}
		} else {
			specialPrice.setVisibility(View.VISIBLE);
			specialPrice.setText(cupon.getSpecialPrice() + Values.EURO_SIGN);
			oldPrice.setText(cupon.getPrice() + Values.EURO_SIGN);
			oldPrice.setPaintFlags(oldPrice.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
			if(oldPriceTitle != null) {
				oldPriceTitle.setText(context.getResources().getString(R.string.money_value));
			}
		}
	}
//	public static void formatMyCuponPriceOutput(TextView oldPrice, TextView specialPrice, Cupon cupon, TextView oldPriceTitle, String discountValue, String value) {
//		if (cupon.getSpecialPrice() == 0.0) {
//			specialPrice.setVisibility(View.GONE);
//			oldPrice.setText(cupon.getPrice() + " " + Values.PROCENT_SIGN);
//			oldPrice.setPaintFlags(oldPrice.getPaintFlags() & (Paint.STRIKE_THRU_TEXT_FLAG));
//			if(oldPriceTitle != null){
//				oldPriceTitle.setText(discountValue);
//			}else{
//				oldPriceTitle.setText(value);
//			} 
//		} else {
//			specialPrice.setText(cupon.getSpecialPrice() + Values.EURO_SIGN);
//			oldPrice.setText(cupon.getPrice() + Values.EURO_SIGN);
//			oldPrice.setPaintFlags(oldPrice.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
//		}
//	}
//
//	public static void formatCuponPriceOutput(TextView oldPrice, TextView specialPrice, Cupon cupon, TextView oldPriceTitle, String discountValue, String value) {
//		if (cupon.getSpecialPrice() == 0.0) {
//			specialPrice.setVisibility(View.GONE);
//			oldPrice.setText(cupon.getPrice() + " " + Values.PROCENT_SIGN);
//			oldPrice.setPaintFlags(oldPrice.getPaintFlags() & (Paint.STRIKE_THRU_TEXT_FLAG));
//			if(!oldPriceTitle.equals(null)){
//				oldPriceTitle.setText(discountValue);
//			}else{
//				oldPriceTitle.setText(value);
//			} 
//		} else {
//			specialPrice.setText(cupon.getSpecialPrice() + Values.EURO_SIGN);
//			oldPrice.setText(cupon.getPrice() + Values.EURO_SIGN);
//			oldPrice.setPaintFlags(oldPrice.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
//		}
//	}
	
	

}
