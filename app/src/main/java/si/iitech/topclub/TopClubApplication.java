package si.iitech.topclub;

import android.app.Application;

import com.google.android.gms.analytics.GoogleAnalytics;
import com.google.android.gms.analytics.Tracker;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.display.FadeInBitmapDisplayer;

public class TopClubApplication extends Application {

	@Override
	public void onCreate() {

		DisplayImageOptions options = new DisplayImageOptions.Builder().showStubImage(R.drawable.no_picture_background)
				.showImageForEmptyUri(R.drawable.no_picture_background).showImageOnFail(R.drawable.no_picture_background).resetViewBeforeLoading()
				.delayBeforeLoading(0).cacheInMemory().cacheOnDisc().displayer(new FadeInBitmapDisplayer(500)).build();

		ImageLoaderConfiguration config = new ImageLoaderConfiguration.Builder(this).threadPoolSize(3).defaultDisplayImageOptions(options).build();
		ImageLoader.getInstance().init(config);
	}

	private Tracker	tracker;

	synchronized Tracker getTracker() {
		if (tracker == null) {
			GoogleAnalytics analytics = GoogleAnalytics.getInstance(this);
			tracker = analytics.newTracker(R.xml.global_tracker);
		}
		return tracker;
	}
}
